var tmpBytes = {audio : {send : 0 , rec : 0} , video : {send : 0 , rec : 0}};
var tmpBandwidth = 0;
//--------------------------------//
var getStatsObj = function(platform,data , callback) {
    var systemNetworkType = ((navigator.connection || {}).type || 'unknown').toString().toLowerCase();

    var getStatsResult = {
        encryption: 'sha-256',
        totalBytesReceived : 0,
        totalBytesSend : 0,
        audioInputLevel : '',
        googLocalAddress : '',
        googRemoteAddress : '',
        packetsSentAudio : 0,
        packetsLostSentAudio : 0,
        packetsRecAudio : 0,
        packetsLostRecAudio : 0,
        packetsSentVideo : 0,
        packetsLostSentVideo : 0,
        packetsRecVideo : 0,
        packetsLostRecVideo : 0,
        //--------------------//
        audio: {
            send: {
                tracks: [],
                codecs: [],
                availableBandwidth: 0,
                streams: 0
            },
            recv: {
                tracks: [],
                codecs: [],
                availableBandwidth: 0,
                streams: 0
            },
            bytesSent: 0,
            bytesReceived: 0
        },
        video: {
            send: {
                tracks: [],
                codecs: [],
                availableBandwidth: 0,
                streams: 0
            },
            recv: {
                tracks: [],
                codecs: [],
                availableBandwidth: 0,
                streams: 0
            },
            bytesSent: 0,
            bytesReceived: 0
        },
        bandwidth: {
            systemBandwidth: 0,
            sentPerSecond: 0,
            encodedPerSecond: 0,
            helper: {
                audioBytesSent: 0,
                videoBytesSent: 0
                
            },
            speed: 0
        },
        results: {},
        connectionType: {
            systemNetworkType: systemNetworkType,
            systemIpAddress: '192.168.1.2',
            local: {
                candidateType: [],
                transport: [],
                ipAddress: [],
                networkType: [],
                iceServer : ''
            },
            remote: {
                candidateType: [],
                transport: [],
                ipAddress: [],
                networkType: [],
                iceServer : ''
            }
        },
        resolutions: {
            send: {
                width: 0,
                height: 0
            },
            recv: {
                width: 0,
                height: 0
            }
        },
        internal: {
            audio: {
                send: {},
                recv: {}
            },
            video: {
                send: {},
                recv: {}
            },
            candidates: {}
        },
        nomore: function() {
            nomore = true;
        }
    };

    var getStatsParser = {
        checkIfOfferer: function(result) {
            if (result.type === 'googLibjingleSession') {
                getStatsResult.isOfferer = result.googInitiator;
            }
        }
    };

    var getStatsParserMoz = {};

    

    var nomore = false;

    function getStatsLooper(data , callback) {
        getStatsWrapper(data , function(results) {
            results.forEach(function(result) {
                if(platform=='chrome'){
                    Object.keys(getStatsParser).forEach(function(key) {
                        if (typeof getStatsParser[key] === 'function') {
                            getStatsParser[key](result);
                        }
                    });
                }else{
                    Object.keys(getStatsParserMoz).forEach(function(key) {
                        if (typeof getStatsParserMoz[key] === 'function') {
                            getStatsParserMoz[key](result);
                        }
                    });
                }
                

                if (result.type !== 'local-candidate' && result.type !== 'remote-candidate' && result.type !== 'candidate-pair') {
                    // console.error('result', result);
                }
            });
            
            if (nomore === true) {
                if (getStatsResult.datachannel) {
                    getStatsResult.datachannel.state = 'close';
                }
                getStatsResult.ended = true;
            }
            if(platform=='chrome'){
                getStatsResult.results = results;
                results.forEach(ele => {
                    if(ele.audioOutputLevel){
                        getStatsResult.audioOutputLevel = ele.audioOutputLevel;
                    }
                    if(ele.googLocalAddress){
                        getStatsResult.googLocalAddress = ele.googLocalAddress;
                    }
                    if(ele.googRemoteAddress){
                        getStatsResult.googRemoteAddress = ele.googRemoteAddress;
                    }
                    if(ele.id.split('_')[2] == 'send' && ele.type == 'ssrc' && ele.mediaType=='audio'){
                        
                        getStatsResult.packetsSentAudio = ele.packetsSent;
                        getStatsResult.packetsLostSendtAudio = ele.packetsLost;

                    }
                    if(ele.id.split('_')[2] == 'recv' && ele.type == 'ssrc' && ele.mediaType=='audio'){
                        // console.log('_send' , ele);
                        getStatsResult.packetsRecAudio = ele.packetsReceived;
                        getStatsResult.packetsLostRecAudio = ele.packetsLost;

                    }
                    if(ele.id.split('_')[2] == 'send' && ele.type == 'ssrc' && ele.mediaType=='video'){
                        getStatsResult.packetsSentVideo = ele.packetsSent;
                        getStatsResult.packetsLostSentVideo = ele.packetsLost;

                    }
                    if(ele.id.split('_')[2] == 'recv' && ele.type == 'ssrc' && ele.mediaType=='video'){
                        // console.log('_send' , ele);
                        getStatsResult.packetsRecVideo = ele.packetsReceived;
                        getStatsResult.packetsLostRecVideo = ele.packetsLost;

                    }
                    
                });


                if((getStatsResult.connectionType.remote.candidateType.indexOf('relayed') !== -1) || (getStatsResult.connectionType.remote.candidateType.indexOf('serverreflexive') !== -1)) {
                    getStatsResult.connectionType.remote.iceServer = 'TURN';
                }
                else {
                    getStatsResult.connectionType.remote.iceServer = 'STUN';
                }
                if((getStatsResult.connectionType.local.candidateType.indexOf('relayed') !== -1) || (getStatsResult.connectionType.local.candidateType.indexOf('serverreflexive') !== -1)) {
                    getStatsResult.connectionType.local.iceServer = 'TURN';
                }
                else {
                    getStatsResult.connectionType.local.iceServer = 'STUN';
                }
                //----------------------------------------//
                if (getStatsResult.audio && getStatsResult.video) {
                        
                        getStatsResult.bandwidth.speed = (getStatsResult.audio.bytesSent - getStatsResult.bandwidth.helper.audioBytesSent) + (getStatsResult.video.bytesSent - getStatsResult.bandwidth.helper.videoBytesSent);
                        // console.log("getStatsResult.bandwidth.speed" , (getStatsResult.audio.bytesSent - getStatsResult.bandwidth.helper.audioBytesSent) + (getStatsResult.video.bytesSent - getStatsResult.bandwidth.helper.videoBytesSent));
                        getStatsResult.bandwidth.helper.audioBytesSent = getStatsResult.audio.bytesSent;
                        getStatsResult.bandwidth.helper.videoBytesSent = getStatsResult.video.bytesSent;
                        
                        // //calculating bandwidth speed persecond
                        // let tmpSpeed = (getStatsResult.audio.bytesSent - getStatsResult.bandwidth.helper.audioBytesSent) + (getStatsResult.video.bytesSent - getStatsResult.bandwidth.helper.videoBytesSent);
                        // let newTmpSpeed = tmpSpeed-tmpBandwidth;
                        // getStatsResult.bandwidth.speed = tmpBandwidth === 0 ? tmpSpeed : newTmpSpeed;
                        // getStatsResult.bandwidth.helper.audioBytesSent = getStatsResult.audio.bytesSent;
                        // getStatsResult.bandwidth.helper.videoBytesSent = getStatsResult.video.bytesSent;
                        // tmpBandwidth = tmpSpeed;


                        // //calculating audio per second
                        // let audioBytesSent = getStatsResult.audio.bytesSent;
                        // getStatsResult.audio.bytesSent = tmpBytes.audio.send === 0 ? audioBytesSent : audioBytesSent-tmpBytes.audio.send;
                        // tmpBytes.audio.send = audioBytesSent;

                        // let audioBytesReceived = getStatsResult.audio.bytesReceived;
                        // getStatsResult.audio.bytesReceived = tmpBytes.audio.rec === 0 ? audioBytesReceived : audioBytesReceived-tmpBytes.audio.rec;
                        // tmpBytes.audio.rec = audioBytesReceived;

                        // //calculating video per second
                        // let tmpvideoBytesSent = getStatsResult.video.bytesSent;
                        // getStatsResult.video.bytesSent = tmpBytes.video.send === 0 ? tmpvideoBytesSent : tmpvideoBytesSent-tmpBytes.video.send;
                        // tmpBytes.video.send = tmpvideoBytesSent;

                        // let videoBytesReceived = getStatsResult.video.bytesReceived;
                        // getStatsResult.video.bytesReceived = tmpBytes.video.rec === 0 ? videoBytesReceived : videoBytesReceived-tmpBytes.video.rec;
                        // tmpBytes.video.rec = videoBytesReceived;

                        //calculating total bytes
                        getStatsResult.totalBytesReceived = getStatsResult.video.bytesReceived+getStatsResult.audio.bytesReceived;
                        getStatsResult.totalBytesSend = getStatsResult.video.bytesSent+getStatsResult.audio.bytesSent;
                        
                    }
            }
            callback(getStatsResult);
            

            
        });
    }

    // a wrapper around getStats which hides the differences (where possible)
    // following code-snippet is taken from somewhere on the github
    function getStatsWrapper(data , cb) {
        // if !peer or peer.signalingState == 'closed' then return;
        // typeof window.InstallTrigger !== 'undefined'
        
        if (platform=='chrome') {
            var items = [];
            data.result().forEach(function(res) {
                var item = {};
                res.names().forEach(function(name) {
                    item[name] = res.stat(name);
                });
                item.id = res.id;
                item.type = res.type;
                item.timestamp = res.timestamp;
                items.push(item);
            });
            cb(items);
            
        } else {
            
            var items = [];
            data.forEach(function(r) {
                items.push(r);
                
            });
            // console.log("getStatsWrapper" , items);
            cb(items);
        }
    };

    getStatsParser.datachannel = function(result) {
        if (result.type !== 'datachannel') return;

        getStatsResult.datachannel = {
            state: result.state // open or connecting
        }
    };

    getStatsParser.googCertificate = function(result) {
        if (result.type == 'googCertificate') {
            getStatsResult.encryption = result.googFingerprintAlgorithm;
        }
    };

    var AUDIO_codecs = ['opus', 'isac', 'ilbc'];

    getStatsParser.checkAudioTracks = function(result) {
        if (!result.googCodecName || result.mediaType !== 'audio') return;

        if (AUDIO_codecs.indexOf(result.googCodecName.toLowerCase()) === -1) return;

        var sendrecvType = result.id.split('_').pop();

        if (getStatsResult.audio[sendrecvType].codecs.indexOf(result.googCodecName) === -1) {
            getStatsResult.audio[sendrecvType].codecs.push(result.googCodecName);
        }

        if (result.bytesSent) {
            var kilobytes = 0;
            if (!!result.bytesSent) {
                if (!getStatsResult.internal.audio[sendrecvType].prevBytesSent) {
                    getStatsResult.internal.audio[sendrecvType].prevBytesSent = result.bytesSent;
                }

                var bytes = result.bytesSent - getStatsResult.internal.audio[sendrecvType].prevBytesSent;
                getStatsResult.internal.audio[sendrecvType].prevBytesSent = result.bytesSent;

                kilobytes = bytes / 1024;
            }

            getStatsResult.audio[sendrecvType].availableBandwidth = kilobytes.toFixed(1);
        }

        if (result.bytesReceived) {
            var kilobytes = 0;
            if (!!result.bytesReceived) {
                if (!getStatsResult.internal.audio[sendrecvType].prevBytesReceived) {
                    getStatsResult.internal.audio[sendrecvType].prevBytesReceived = result.bytesReceived;
                }

                var bytes = result.bytesReceived - getStatsResult.internal.audio[sendrecvType].prevBytesReceived;
                getStatsResult.internal.audio[sendrecvType].prevBytesReceived = result.bytesReceived;

                kilobytes = bytes / 1024;
            }

            getStatsResult.audio[sendrecvType].availableBandwidth = kilobytes.toFixed(1);
        }

        if (getStatsResult.audio[sendrecvType].tracks.indexOf(result.googTrackId) === -1) {
            getStatsResult.audio[sendrecvType].tracks.push(result.googTrackId);
        }
    };

    var VIDEO_codecs = ['vp9', 'vp8', 'h264'];

    getStatsParser.checkVideoTracks = function(result) {
        if (!result.googCodecName || result.mediaType !== 'video') return;

        if (VIDEO_codecs.indexOf(result.googCodecName.toLowerCase()) === -1) return;

        // googCurrentDelayMs, googRenderDelayMs, googTargetDelayMs
        // transportId === 'Channel-audio-1'
        var sendrecvType = result.id.split('_').pop();

        if (getStatsResult.video[sendrecvType].codecs.indexOf(result.googCodecName) === -1) {
            getStatsResult.video[sendrecvType].codecs.push(result.googCodecName);
        }

        if (!!result.bytesSent) {
            var kilobytes = 0;
            if (!getStatsResult.internal.video[sendrecvType].prevBytesSent) {
                getStatsResult.internal.video[sendrecvType].prevBytesSent = result.bytesSent;
            }

            var bytes = result.bytesSent - getStatsResult.internal.video[sendrecvType].prevBytesSent;
            getStatsResult.internal.video[sendrecvType].prevBytesSent = result.bytesSent;

            kilobytes = bytes / 1024;
        }

        if (!!result.bytesReceived) {
            var kilobytes = 0;
            if (!getStatsResult.internal.video[sendrecvType].prevBytesReceived) {
                getStatsResult.internal.video[sendrecvType].prevBytesReceived = result.bytesReceived;
            }

            var bytes = result.bytesReceived - getStatsResult.internal.video[sendrecvType].prevBytesReceived;
            getStatsResult.internal.video[sendrecvType].prevBytesReceived = result.bytesReceived;

            kilobytes = bytes / 1024;
        }

        getStatsResult.video[sendrecvType].availableBandwidth = kilobytes.toFixed(1);

        if (result.googFrameHeightReceived && result.googFrameWidthReceived) {
            getStatsResult.resolutions[sendrecvType].width = result.googFrameWidthReceived;
            getStatsResult.resolutions[sendrecvType].height = result.googFrameHeightReceived;
        }

        if (result.googFrameHeightSent && result.googFrameWidthSent) {
            getStatsResult.resolutions[sendrecvType].width = result.googFrameWidthSent;
            getStatsResult.resolutions[sendrecvType].height = result.googFrameHeightSent;
        }

        if (getStatsResult.video[sendrecvType].tracks.indexOf(result.googTrackId) === -1) {
            getStatsResult.video[sendrecvType].tracks.push(result.googTrackId);
        }
    };

    getStatsParser.bweforvideo = function(result) {
        if (result.type !== 'VideoBwe') return;

        getStatsResult.bandwidth.availableSendBandwidth = result.googAvailableSendBandwidth;

        getStatsResult.bandwidth.googActualEncBitrate = result.googActualEncBitrate;
        getStatsResult.bandwidth.googAvailableSendBandwidth = result.googAvailableSendBandwidth;
        getStatsResult.bandwidth.googAvailableReceiveBandwidth = result.googAvailableReceiveBandwidth;
        getStatsResult.bandwidth.googRetransmitBitrate = result.googRetransmitBitrate;
        getStatsResult.bandwidth.googTargetEncBitrate = result.googTargetEncBitrate;
        getStatsResult.bandwidth.googBucketDelay = result.googBucketDelay;
        getStatsResult.bandwidth.googTransmitBitrate = result.googTransmitBitrate;
    };

    getStatsParser.candidatePair = function(result) {
        if (result.type !== 'googCandidatePair' && result.type !== 'candidate-pair') return;

        // result.googActiveConnection means either STUN or TURN is used.

        if (result.googActiveConnection == 'true') {
            // id === 'Conn-audio-1-0'
            // localCandidateId, remoteCandidateId

            // bytesSent, bytesReceived

            Object.keys(getStatsResult.internal.candidates).forEach(function(cid) {
                var candidate = getStatsResult.internal.candidates[cid];
                if (candidate.ipAddress.indexOf(result.googLocalAddress) !== -1) {
                    getStatsResult.connectionType.local.candidateType = candidate.candidateType;
                    getStatsResult.connectionType.local.ipAddress = candidate.ipAddress;
                    getStatsResult.connectionType.local.networkType = candidate.networkType;
                    getStatsResult.connectionType.local.transport = candidate.transport;
                }
                if (candidate.ipAddress.indexOf(result.googRemoteAddress) !== -1) {
                    getStatsResult.connectionType.remote.candidateType = candidate.candidateType;
                    getStatsResult.connectionType.remote.ipAddress = candidate.ipAddress;
                    getStatsResult.connectionType.remote.networkType = candidate.networkType;
                    getStatsResult.connectionType.remote.transport = candidate.transport;
                }
            });

            getStatsResult.connectionType.transport = result.googTransportType;

            var localCandidate = getStatsResult.internal.candidates[result.localCandidateId];
            if (localCandidate) {
                if (localCandidate.ipAddress) {
                    getStatsResult.connectionType.systemIpAddress = localCandidate.ipAddress;
                }
            }

            var remoteCandidate = getStatsResult.internal.candidates[result.remoteCandidateId];
            if (remoteCandidate) {
                if (remoteCandidate.ipAddress) {
                    getStatsResult.connectionType.systemIpAddress = remoteCandidate.ipAddress;
                }
            }
        }

        if (result.type === 'candidate-pair') {
            if (result.selected === true && result.nominated === true && result.state === 'succeeded') {
                // remoteCandidateId, localCandidateId, componentId
                var localCandidate = getStatsResult.internal.candidates[result.remoteCandidateId];
                var remoteCandidate = getStatsResult.internal.candidates[result.remoteCandidateId];

                // Firefox used above two pairs for connection
            }
        }
    };

    var LOCAL_candidateType = {};
    var LOCAL_transport = {};
    var LOCAL_ipAddress = {};
    var LOCAL_networkType = {};

    getStatsParser.localcandidate = function(result) {
        if (result.type !== 'localcandidate' && result.type !== 'local-candidate') return;
        if (!result.id) return;
        // console.log("remote-candidate" , result);
        if (!LOCAL_candidateType[result.id]) {
            LOCAL_candidateType[result.id] = [];
        }

        if (!LOCAL_transport[result.id]) {
            LOCAL_transport[result.id] = [];
        }

        if (!LOCAL_ipAddress[result.id]) {
            LOCAL_ipAddress[result.id] = [];
        }

        if (!LOCAL_networkType[result.id]) {
            LOCAL_networkType[result.id] = [];
        }

        if (result.candidateType && LOCAL_candidateType[result.id].indexOf(result.candidateType) === -1) {
            LOCAL_candidateType[result.id].push(result.candidateType);
        }

        if (result.transport && LOCAL_transport[result.id].indexOf(result.transport) === -1) {
            LOCAL_transport[result.id].push(result.transport);
        }

        if (result.ipAddress && LOCAL_ipAddress[result.id].indexOf(result.ipAddress + ':' + result.portNumber) === -1) {
            LOCAL_ipAddress[result.id].push(result.ipAddress + ':' + result.portNumber);
        }

        if (result.networkType && LOCAL_networkType[result.id].indexOf(result.networkType) === -1) {
            LOCAL_networkType[result.id].push(result.networkType);
        }

        getStatsResult.internal.candidates[result.id] = {
            candidateType: LOCAL_candidateType[result.id],
            ipAddress: LOCAL_ipAddress[result.id],
            portNumber: result.portNumber,
            networkType: LOCAL_networkType[result.id],
            priority: result.priority,
            transport: LOCAL_transport[result.id],
            timestamp: result.timestamp,
            id: result.id,
            type: result.type
        };

        getStatsResult.connectionType.local.candidateType = LOCAL_candidateType[result.id];
        getStatsResult.connectionType.local.ipAddress = LOCAL_ipAddress[result.id];
        getStatsResult.connectionType.local.networkType = LOCAL_networkType[result.id];
        getStatsResult.connectionType.local.transport = LOCAL_transport[result.id];
    };

    var REMOTE_candidateType = {};
    var REMOTE_transport = {};
    var REMOTE_ipAddress = {};
    var REMOTE_networkType = {};

    getStatsParser.remotecandidate = function(result) {
        if (result.type !== 'remotecandidate' && result.type !== 'remote-candidate') return;
        if (!result.id) return;

        if (!REMOTE_candidateType[result.id]) {
            REMOTE_candidateType[result.id] = [];
        }

        if (!REMOTE_transport[result.id]) {
            REMOTE_transport[result.id] = [];
        }

        if (!REMOTE_ipAddress[result.id]) {
            REMOTE_ipAddress[result.id] = [];
        }

        if (!REMOTE_networkType[result.id]) {
            REMOTE_networkType[result.id] = [];
        }

        if (result.candidateType && REMOTE_candidateType[result.id].indexOf(result.candidateType) === -1) {
            REMOTE_candidateType[result.id].push(result.candidateType);
        }

        if (result.transport && REMOTE_transport[result.id].indexOf(result.transport) === -1) {
            REMOTE_transport[result.id].push(result.transport);
        }

        if (result.ipAddress && REMOTE_ipAddress[result.id].indexOf(result.ipAddress + ':' + result.portNumber) === -1) {
            REMOTE_ipAddress[result.id].push(result.ipAddress + ':' + result.portNumber);
        }

        if (result.networkType && REMOTE_networkType[result.id].indexOf(result.networkType) === -1) {
            REMOTE_networkType[result.id].push(result.networkType);
        }

        getStatsResult.internal.candidates[result.id] = {
            candidateType: REMOTE_candidateType[result.id],
            ipAddress: REMOTE_ipAddress[result.id],
            portNumber: result.portNumber,
            networkType: REMOTE_networkType[result.id],
            priority: result.priority,
            transport: REMOTE_transport[result.id],
            timestamp: result.timestamp,
            id: result.id,
            type: result.type
        };

        getStatsResult.connectionType.remote.candidateType = REMOTE_candidateType[result.id];
        getStatsResult.connectionType.remote.ipAddress = REMOTE_ipAddress[result.id];
        getStatsResult.connectionType.remote.networkType = REMOTE_networkType[result.id];
        getStatsResult.connectionType.remote.transport = REMOTE_transport[result.id];
    };

    getStatsParser.dataSentReceived = function(result) {
        
        if (!result.googCodecName || (result.mediaType !== 'video' && result.mediaType !== 'audio')) return;

        if (!!result.bytesSent) {
            getStatsResult[result.mediaType].bytesSent = parseInt(result.bytesSent);
        }

        if (!!result.bytesReceived) {
            // console.log("getStatsResult[result.mediaType]" , result.mediaType , result.googCodecName ,result.type,result);
            getStatsResult[result.mediaType].bytesReceived = parseInt(result.bytesReceived);
        }
    };

    var SSRC = {
        audio: {
            send: [],
            recv: []
        },
        video: {
            send: [],
            recv: []
        }
    };

    getStatsParser.ssrc = function(result) {
        if (!result.googCodecName || (result.mediaType !== 'video' && result.mediaType !== 'audio')) return;
        if (result.type !== 'ssrc') return;
        var sendrecvType = result.id.split('_').pop();

        if (SSRC[result.mediaType][sendrecvType].indexOf(result.ssrc) === -1) {
            SSRC[result.mediaType][sendrecvType].push(result.ssrc)
        }

        getStatsResult[result.mediaType][sendrecvType].streams = SSRC[result.mediaType][sendrecvType].length;
    };









    //=================Mozilla=================//
    getStatsParserMoz.checkAudioStuff = function(result) {
        getStatsResult.audio.send.codecs = 'NA';
        getStatsResult.audio.recv.codecs = 'NA';
        getStatsResult.audioOutputLevel = 'NA';
        if (result.mediaType == 'audio' && result.isRemote === false){
            
            switch (result.type) {
                case  'inbound-rtp' :
                    // console.log("inbound-rtp audio" , result);
                    getStatsResult.audio.bytesReceived = result.bytesReceived;
                    getStatsResult.packetsRecAudio = result.packetsReceived;
                    getStatsResult.packetsLostRecAudio = result.packetsLost;
                break;
                case 'outbound-rtp' :
                    // console.log("outbound-rtp audio" , result);
                    getStatsResult.audio.bytesSent=result.bytesSent;
                    getStatsResult.packetsSentAudio = result.packetsSent;
                break;
                
            }
        }

        
    };
    getStatsParserMoz.checkVideoStuff = function(result) {
        getStatsResult.video.send.codecs = 'NA';
        getStatsResult.video.recv.codecs = 'NA';
        if (result.mediaType == 'video' && result.isRemote === false){
            switch (result.type) {
                case  'inbound-rtp' :
                    // console.log("inbound-rtp video" , result);
                    getStatsResult.video.bytesReceived = result.bytesReceived;
                    getStatsResult.packetsRecVideo = result.packetsReceived;
                    getStatsResult.packetsLostRecAudio = result.packetsLost;
                break;
                case 'outbound-rtp' :
                    // console.log("outbound-rtp video" , result);
                    getStatsResult.video.bytesSent=result.bytesSent;
                    getStatsResult.packetsSentVideo = result.packetsSent;
                break;
            }
        }
    };
    var LOCAL_candidateType_moz = {};
    var LOCAL_transport_moz = {};
    var LOCAL_ipAddress_moz = {};
    var LOCAL_networkType_moz = {};

    var REMOTE_candidateType_moz = {};
    var REMOTE_transport_moz = {};
    var REMOTE_ipAddress_moz = {};
    var REMOTE_networkType_moz = {};
    getStatsParserMoz.connectionStuff = function(result){
        switch (result.type) {
            case 'candidate-pair' :
                // console.log("candidate-pair" , result);
            break;
            case 'local-candidate' :
                // console.log("local-candidate" , result);

                getStatsResult.connectionType.local.candidateType = [];
                getStatsResult.connectionType.local.ipAddress = [];
                getStatsResult.connectionType.local.networkType = [];
                getStatsResult.connectionType.local.transport = [];
                getStatsResult.googLocalAddress = '';
                getStatsResult.connectionType.local.iceServer = '';

                if (!LOCAL_candidateType_moz[result.id]) {
                    LOCAL_candidateType_moz[result.id] = [];
                }
        
                if (!LOCAL_transport_moz[result.id]) {
                    LOCAL_transport_moz[result.id] = [];
                }
        
                if (!LOCAL_ipAddress_moz[result.id]) {
                    LOCAL_ipAddress_moz[result.id] = [];
                }
        
                if (!LOCAL_networkType_moz[result.id]) {
                    LOCAL_networkType_moz[result.id] = [];
                }
        
                if (result.candidateType && LOCAL_candidateType_moz[result.id].indexOf(result.candidateType) === -1) {
                    LOCAL_candidateType_moz[result.id].push(result.candidateType);
                }
        
                if (result.transport && LOCAL_transport_moz[result.id].indexOf(result.transport) === -1) {
                    LOCAL_transport_moz[result.id].push(result.transport);
                }
        
                if (result.ipAddress && LOCAL_ipAddress_moz[result.id].indexOf(result.ipAddress + ':' + result.portNumber) === -1) {
                    LOCAL_ipAddress_moz[result.id].push(result.ipAddress + ':' + result.portNumber);
                }
        
                if (result.networkType && LOCAL_networkType_moz[result.id].indexOf(result.networkType) === -1) {
                    LOCAL_networkType_moz[result.id].push(result.networkType);
                }
        
                getStatsResult.connectionType.local.candidateType = LOCAL_candidateType_moz[result.id];
                getStatsResult.connectionType.local.ipAddress = LOCAL_ipAddress_moz[result.id];
                getStatsResult.connectionType.local.networkType = LOCAL_networkType_moz[result.id];
                getStatsResult.connectionType.local.transport = LOCAL_transport_moz[result.id];

                getStatsResult.googLocalAddress = getStatsResult.connectionType.local.ipAddress;
                if((getStatsResult.connectionType.local.candidateType.indexOf('relayed') !== -1) || (getStatsResult.connectionType.local.candidateType.indexOf('serverreflexive') !== -1)){
                    getStatsResult.connectionType.local.iceServer = 'TURN';
                }else{
                    getStatsResult.connectionType.local.iceServer = 'STUN';
                }
            break;
            case 'remote-candidate' :
                // console.log("remote-candidate" , result);
                getStatsResult.connectionType.remote.candidateType = [];
                getStatsResult.connectionType.remote.ipAddress = [];
                getStatsResult.connectionType.remote.networkType = [];
                getStatsResult.connectionType.remote.transport = [];

                getStatsResult.googRemoteAddress = '';
                getStatsResult.connectionType.remote.iceServer = '';

                if (!REMOTE_candidateType_moz[result.id]) {
                    REMOTE_candidateType_moz[result.id] = [];
                }
        
                if (!REMOTE_transport_moz[result.id]) {
                    REMOTE_transport_moz[result.id] = [];
                }
        
                if (!REMOTE_ipAddress_moz[result.id]) {
                    REMOTE_ipAddress_moz[result.id] = [];
                }
        
                if (!REMOTE_networkType_moz[result.id]) {
                    REMOTE_networkType_moz[result.id] = [];
                }
        
                if (result.candidateType && REMOTE_candidateType_moz[result.id].indexOf(result.candidateType) === -1) {
                    REMOTE_candidateType_moz[result.id].push(result.candidateType);
                }
        
                if (result.transport && REMOTE_transport_moz[result.id].indexOf(result.transport) === -1) {
                    REMOTE_transport_moz[result.id].push(result.transport);
                }
        
                if (result.ipAddress && REMOTE_ipAddress_moz[result.id].indexOf(result.ipAddress + ':' + result.portNumber) === -1) {
                    REMOTE_ipAddress_moz[result.id].push(result.ipAddress + ':' + result.portNumber);
                }
        
                if (result.networkType && REMOTE_networkType_moz[result.id].indexOf(result.networkType) === -1) {
                    REMOTE_networkType_moz[result.id].push(result.networkType);
                }
        
                getStatsResult.connectionType.remote.candidateType = REMOTE_candidateType_moz[result.id];
                getStatsResult.connectionType.remote.ipAddress = REMOTE_ipAddress_moz[result.id];
                getStatsResult.connectionType.remote.networkType = REMOTE_networkType_moz[result.id];
                getStatsResult.connectionType.remote.transport = REMOTE_transport_moz[result.id];

                getStatsResult.googRemoteAddress = getStatsResult.connectionType.remote.ipAddress;
                if((getStatsResult.connectionType.remote.candidateType.indexOf('relayed') !== -1) || (getStatsResult.connectionType.remote.candidateType.indexOf('serverreflexive') !== -1)){
                    getStatsResult.connectionType.remote.iceServer = 'TURN';
                }else{
                    getStatsResult.connectionType.remote.iceServer = 'STUN';
                }
            break;
        }
     }
    //=========================================//



    getStatsLooper(data,callback);

};


