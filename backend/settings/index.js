require('dotenv').config();
const common = require('./common');

const SITE_ENV = process.env.SITE_ENV;
console.log(SITE_ENV);

let setting = {};
if(SITE_ENV === 'localhost') {
	setting = require('./config-local');
} else if (SITE_ENV === 'stag') {
	setting = require('./config-stag');
} else if (SITE_ENV === 'prod') {
	setting = require('./config-prod');
} else {
	throw new Error('Enviroment is not valid');
}
function extend(obj, src) {
    for (var key in src) {
        if (src.hasOwnProperty(key)) obj[key] = src[key];
    }
    return obj;
}
module.exports = extend(setting, common);