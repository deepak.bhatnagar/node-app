const path = require("path");
let config = {};
config.PORT = 61289;
config.HTTP_PORT = 61290;
config.ssl = true;
config.host = "localhost";
config.allowOriginSocket = "*:*";
if (config.ssl) {
  config.baseUrl = `https://${config.host}:${config.PORT}`;
  config.allowOriginWeb = [config.baseUrl, "https://" + config.host + ":4200"];
} else {
  config.baseUrl = `http://${config.host}:${config.PORT}`;
  config.allowOriginWeb = [config.baseUrl, "http://" + config.host + ":4200"];
}
config.contactEmails = ["info@galaxyinfotech.co"];
config.sslSetting = {
  key: "fake-keys/privatekey.pem",
  cert: "fake-keys/certificate.pem"
};
config.iceServerDetails = {
  host: "api.twilio.com",
  path: "/2010-04-01/Accounts/ACa0381f7be45a0f16bdea2e3be2e425ed/Tokens.json",
  method: "POST",
  headers: {
    Authorization:
      "Basic " +
      new Buffer(
        "ACa0381f7be45a0f16bdea2e3be2e425ed:2246fe7562e4126917fbaa0cf1662205"
      ).toString("base64")
  }
};
config.nodeMailerOptoins = {
  host: "secure.emailsrvr.com",
  port: 465,
  secure: true,
  auth: { user: "galaxy-test@galaxyinfotech.co", pass: "Galaxy@3456GWL" }
};

config.rootpath = path.resolve(".");
config.logspath = path.resolve(".") + "/public/logs";

module.exports = config;
