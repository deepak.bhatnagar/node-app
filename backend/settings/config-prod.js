const path = require("path");
let config = {};
config.live = true;
config.ssl = false;
config.PORT = 61289;
config.HTTP_PORT = 61290;
config.host = "localhost";
config.allowOriginSocket = "*:*";
let ssl = config.ssl ? "https" : "http";
config.baseUrl = ssl + "://" + config.host;
config.allowOriginWeb = [config.baseUrl, ssl + "://" + config.host + ":4200"];
config.contactEmails = ["info@galaxyinfotech.co","varun@rtcweb.in","amit.bukhariya@galaxyweblinks.co.in","pranshu.joshi@galaxyweblinks.in","lucas.pereira@galaxyweblinks.com","lucas@rtcweb.in"];

if (config.live) {
  config.sslSetting = {
    key: "keys/server.key",
    cert: "keys/server.crt",
    ca: "keys/server.ca-bundle"
  };
  config.iceServerDetails = {
    host: "api.twilio.com",
    path:
      "/2010-04-01/Accounts/AC1f9a0529bf8a2b64a5df5c1f9efba262/Tokens.json",
    method: "POST",
    headers: {
      Authorization:
        "Basic " +
        new Buffer(
          "AC1f9a0529bf8a2b64a5df5c1f9efba262:b0b037bebae03f6d22ec75e3faf6bd1f"
        ).toString("base64")
    }
  };
  config.nodeMailerOptoins = {
    host: "smtp.sendgrid.net",
    port: 465 ,
    secure: true,
    auth: { user: "talkroom", pass: "supp0rt@123" }
  };
} else {
  config.sslSetting = {
    key: "fake-keys/privatekey.pem",
    cert: "fake-keys/certificate.pem"
  };
  config.iceServerDetails = {
    host: "api.twilio.com",
    path: "/2010-04-01/Accounts/ACa0381f7be45a0f16bdea2e3be2e425ed/Tokens.json",
    method: "POST",
    headers: {
      Authorization:
        "Basic " +
        new Buffer(
          "ACa0381f7be45a0f16bdea2e3be2e425ed:2246fe7562e4126917fbaa0cf1662205"
        ).toString("base64")
    }
  };
  config.nodeMailerOptoins = {
    host: "secure.emailsrvr.com",
    port: 465,
    secure: true,
    auth: { user: "galaxy-test@galaxyinfotech.co", pass: "Galaxy@3456GWL" }
  };
}

config.rootpath = path.resolve(".");
config.logspath = path.resolve(".") + "/public/logs";

module.exports = config;
