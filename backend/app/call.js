
'use strict';

const logger = require('./log');
const utility = require('./utility');
var rooms = {};

class Call {

    constructor() {

    }

    joinRoom(socket, data, fn) {
        logger.info(socket.customUniqueId , 'join', socket.roomid);
        rooms[socket.roomid] = rooms[socket.roomid] || {};
        if( !utility.isEmpty(rooms[socket.roomid]) ) {
            let edata = { id: socket.customUniqueId, data: data };
            logger.info(socket.roomid, '-peer.connected-', edata );
            socket.broadcast.to(socket.roomid).emit('peer.connected', edata );
        }
        //call before to prevent to store selfdata from its client
        fn(rooms[socket.roomid]);
        rooms[socket.roomid][socket.customUniqueId] = data;
    }

    callEvents(io) {
        let socketIds = {};
        io.on('connection', (socket) => {
            //console.log('connect' , socket.id);
            var customUniqueId, roomId;

            console.log('socket.handshake.query',socket.handshake.query);
            let query = socket.handshake.query;
            customUniqueId = socket.customUniqueId = query.customUniqueId;
            socketIds[customUniqueId] = socket.id;

            console.log('customUniqueId', customUniqueId);


            socket.on("isExist", ( roomid, fn ) => {
                try {
                    fn( rooms && rooms[roomid] );
                }catch(e) {
                    logger.log({level: 'error', message: e});
                }
            });

            socket.on("join", ( roomid, data ,fn ) => {
                try {
                    socket.roomid = roomid;
                    this.joinRoom(socket , data, fn);
                    socket.join(socket.roomid);
                }catch(e) {
                    logger.log({level: 'error', message: e});
                }
            });
            socket.on("joinStream", ( streamid, data ,fn ) => {
                logger.info(socket.roomid, '-joinStream-', streamid );
                try {
                    rooms[socket.roomid] = rooms[socket.roomid] || {};
                    if( !utility.isEmpty(rooms[socket.roomid]) ) {
                        let edata = { id: customUniqueId, streamid:streamid, data: data };
                        logger.info(socket.roomid, '-stream.connected-', edata );
                        socket.broadcast.to(socket.roomid).emit('stream.connected', edata );
                    }
                    rooms[socket.roomid][customUniqueId] = data;
                    fn(rooms[socket.roomid]);

                }catch(e) {
                    logger.log({level: 'error', message: e});
                }
            });
            socket.on("updatedata", (data) => {
                try {
                    rooms[socket.roomid][customUniqueId] = rooms[socket.roomid][customUniqueId] || {};
                    rooms[socket.roomid][customUniqueId] = data;
                    let edata = { id: customUniqueId, data: data };
                    logger.info(socket.roomid, '-updatedata-', edata );
                    socket.broadcast.to(socket.roomid).emit('updatedata', edata);
                }catch(e) {
                    logger.log({level: 'error', message: e});
                }
            });

            socket.on("removeStream", (streamid, data) => {
                try {
                    rooms[socket.roomid][customUniqueId] = rooms[socket.roomid][customUniqueId] || {};
                    rooms[socket.roomid][customUniqueId] = data;
                    let edata = { id: customUniqueId, streamid:streamid , data: data };
                    logger.info(socket.roomid, '-stream.removed-', edata );
                    socket.broadcast.to(socket.roomid).emit('stream.removed', edata);
                }catch(e) {
                    logger.log({level: 'error', message: e});
                }
            });

            socket.on('signalling', (data) => {
                try {
                    if (rooms[socket.roomid] && rooms[socket.roomid][data.to]) {
                        logger.info(socket.roomid, '-signalling-');
                        socket.broadcast.to( socketIds[data.to] ).emit('signalling', data);
                    } else {
                        console.error('Invalid user: signalling', data);
                    }
                }
                catch(e) {
                    logger.log({level: 'error', message: e});
                }
            });

            socket.on('switching', (records) => {
                try {
                    rooms[socket.roomid][customUniqueId] = records.data;
                    logger.info(socket.roomid, '-switching-', records );
                    socket.broadcast.to(socket.roomid).emit('switching', records);
                }
                catch(e) {
                    logger.log({level: 'error', message: e});
                }
            });

            socket.on('notifications', (records) => {
                try {
                    if(socket.roomid)
                        socket.broadcast.to(socket.roomid).emit('notifications', records);
                }
                catch(e) {
                    logger.log({level: 'error', message: e});
                }
            });

            // when the client emits 'typing', we broadcast it to others
            socket.on('typing', function (data) {
                try {
                    if(socket.roomid)
                        socket.broadcast.to(socket.roomid).emit('typing', data);
                }
                catch(e) {
                    logger.log({level: 'error', message: e});
                }
            });

            socket.on("disconnect", () => {
                try {
                    if(socket && socket.roomid ) {
                        let edata = { id:customUniqueId, data:rooms[socket.roomid][customUniqueId] };
                        logger.info(socket.roomid, '-peer.disconnected-', edata );
                        socket.broadcast.to(socket.roomid).emit('peer.disconnected', edata);
                        delete rooms[socket.roomid][customUniqueId];
                        if(utility.isEmpty(rooms[socket.roomid])) {
                            delete rooms[socket.roomid];
                        }
                    }
                    if(socketIds[customUniqueId] != undefined){
                        delete socketIds[customUniqueId];
                    }
                }catch(e) {
                    logger.log({level: 'error', message: e});
                } 
            });
        });
    }
}

module.exports = new Call();
