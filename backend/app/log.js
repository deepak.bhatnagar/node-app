const winston = require('winston');
const fs = require('fs');
const __logpath = require('../settings').logspath;

if (!fs.existsSync(__logpath)){
    fs.mkdirSync(__logpath);
}
const suffix = new Date().toISOString().replace(/T.*/,'').split('-').reverse().join('-');
const logger = new (winston.Logger)({
  transports: [
    new (winston.transports.Console)({ json: false, timestamp: true }),
    new winston.transports.File({ filename: __logpath + `/debug-${suffix}.log`, json: false })
  ],
  exceptionHandlers: [
    new (winston.transports.Console)({ json: false, timestamp: true }),
    new winston.transports.File({ filename: __logpath + `/error-${suffix}.log`, json: false })
  ],
  exitOnError: false
});

module.exports = logger;
