const https = require("https"),
    utilJs = require('./utilFunctions'),
    config = require('../../settings');

/***
 * Name: getStunTurnServerUrls
 * Method:POST
 * Params:{}
 * return: list of stun turn server
 */

exports.getStunTurnServerUrls = (req, res, next) => {

    
    /*var options = {
        host: "api.twilio.com",
        path: "/2010-04-01/Accounts/AC1548e5e14ab9ef52f9570050c0a29e8d/Tokens.json",
        method: "POST",
        headers: {
            "Authorization": "Basic " + new Buffer("AC1548e5e14ab9ef52f9570050c0a29e8d:b7aadedc939d36839ddc51392bb469b3").toString("base64") 
        }
    };*/

    var options = config.iceServerDetails;
    let response = {
        status: false,
        msg: "success",
        code: 200,
        result: {}
    };

    var httpreq = https.request(options, (httpres) => {
        var str = "";
         console.log(str);
        httpres.on("data", (data) => {
            str += data;
        });

        httpres.on("error", (e) => {
            //console.log("error: ", e);          
            res.json(utilJs.formatedResponse(false,[],"Failed to fetch records."));
        });
        httpres.on("end", () => {
            // response.status = true;
            // response.code = 200;
            // response.msg = "success";
            // response.result = JSON.parse(str).v;
            console.log(str);
            res.json(utilJs.formatedResponse(true, JSON.parse(str).ice_servers,"success"));
        });
    });
    httpreq.end();
} //getStunTurnServerUrls

exports.contactUs = (req, res) => {
    utilJs.sendMail(req.body.reqParam, (err, response) => {
        if (err) {
           // console.log('error', 'contactUs', response);           
            res.status(200).json(utilJs.formatedResponse(false,[],'fail'));
        } 
        res.status(200).json(utilJs.formatedResponse(true,[],response.message));
    });
};

