
'use strict';

const nodemailer = require("nodemailer"),
	config = require('../../settings');

class Mailer{
	constructor() {
		/* this.smtpTransport = nodemailer.createTransport({
												service: "sendgrid",
												host: "smtp.sendgrid.net",
												auth: { user: "kpmrs", pass: "Kpmrs!@#" }
											}); */
		this.smtpTransport = nodemailer.createTransport(config.nodeMailerOptoins);
	}
}
let mailer = new Mailer()
module.exports = mailer.smtpTransport;
