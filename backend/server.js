'use strict';

// Bring in our dependencies
const express = require('express');
const bodyParser = require('body-parser');
const socketio = require('socket.io');
const fs = require('fs');
const path = require('path');
const config = require('./settings');
const ssl = config.ssl;
const Call = require('./app/call');
const apis = require('./app/controller/apis');
const allowedExt = config.allowedExt;

class Server {

    constructor() {
        this.port =  process.env.PORT || config.PORT;
        this.app = express();

        // Redirect from http to https 
        var http = require('http'); 
        http.createServer(function (req, res) { 
            console.log("https://" + config.host+":"+config.PORT+ req.url);
            res.writeHead(301, { "Location": "https://" + config.host+":"+config.PORT+ req.url }); 
            res.end(); 
        }).listen(config.HTTP_PORT);

        if( ssl) {
            var options = {};
            for( let key in config.sslSetting) {
                if( config.sslSetting.hasOwnProperty(key) ){
                    options[key] = fs.readFileSync(config.sslSetting[key]);
                }
            }
            this.http = require('https').createServer(options, this.app);
        }else{
            this.http = require('http').Server(this.app);
        }
        this.io = socketio(this.http);
        this.nsp = this.io.of('/call')

        this.io.set('origins', config.allowOriginSocket);

    }

    appConfig(){
        // ---------------------------------------START CODE TO REMOVE # FROM URL ------------------------------------------ //
        // Redirect all the other requests
        this.app.get('*', (req, res) => {
            req.url = req.url.split("?").shift(); // remove query strings from static resources
            let extArr = allowedExt.filter(ext => req.url.search(ext) > -1);
            if (extArr.length > 0) {
                res.sendFile(path.resolve(`public/${req.url}`));
            } else {
                console.log(req.url)
                if(req.url === '/apple-app-site-association'){
                    res.sendFile(path.resolve(`files/${req.url}`));
                }else{
                    res.sendFile(path.resolve('public/index.html'));
                }
            }
          });
        // -----------------------------------------END CODE TO REMOVE # FROM URL ------------------------------------------- //

        this.app.use(function (req, res, next) {
            if(config.allowOriginWeb.indexOf(req.headers.origin) > -1){
                res.setHeader('Access-Control-Allow-Origin', req.headers.origin);
            }
            res.setHeader('Access-Control-Allow-Headers', 'content-type, x-access-token');
            next();
        });

        this.app.use(express.static(__dirname + '/public'));

        this.app.use( bodyParser.json() ); // support json encoded bodies
        this.app.use( bodyParser.urlencoded( { extended: true } ) );

        // set the view engine to ejs
        this.app.set('view engine', 'ejs');


    }

    /* Including app Routes starts*/
    includeRoutes(){
        Call.callEvents(this.nsp);
    }
    /* Including app Routes ends*/  

    appExecute(){

        this.appConfig();

        this.app.post('/getIceServer',apis.getStunTurnServerUrls);
        this.app.post('/contactUs',apis.contactUs);

        this.includeRoutes();
        this.http.listen(this.port, () => {
            console.log(`App listening on port ${this.port}`);
        });

    }

}


const app = new Server();
app.appExecute();
