1.	Angular migration 4 to 7 done
2.	Webrtc.js up-gradation in context of socket.io flow
3.	improved code quality using VS extension (ESLint, TSLint) inProgress
4.	simulating bug fixes for the new browser
5.	Adding file for universal linking
6.	Remove Hash from URls
7.	Plugin free Screen Sharing 
8.	Component based development


Remaining

1.	Linting
2.	Code commenting
3.	Loose coupling
4.	Safari (safari under review )
5.	Documentation folder 
6.	WebRTC 1.0 unifier plan study