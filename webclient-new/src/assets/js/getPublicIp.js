function getIPs(callback) {
  try {
    var ip_dups = {};
    //compatibility for firefox and chrome
    var RTCPeerConnection = window.RTCPeerConnection ||
      window.mozRTCPeerConnection ||
      window.webkitRTCPeerConnection;
    var useWebKit = !!window.webkitRTCPeerConnection;
    if (!RTCPeerConnection) {
      var ifrm = document.createElement('iframe');
      ifrm.setAttribute('id', 'iframe'); // assign an id
      ifrm.setAttribute('sandbox', 'allow-same-origin'); // assign an id
      ifrm.style.display = 'none';

      var win = ifrm.contentWindow;
      //var win = iframe.contentWindow;
      RTCPeerConnection = win.RTCPeerConnection ||
        win.mozRTCPeerConnection ||
        win.webkitRTCPeerConnection;
      useWebKit = !!win.webkitRTCPeerConnection;
    }
    var mediaConstraints = {
      optional: [{
        RtpDataChannels: false
      }]
    };
    var servers = {
      iceServers: [{
        urls: 'stun:stun.l.google.com:19302'
      }]
    };
    //construct a new RTCPeerConnection
    var pc = new RTCPeerConnection(servers, mediaConstraints);

    function handleCandidate(candidate) {
      //match just the IP address
      var ip_regex = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/
      var ip_addr = ip_regex.exec(candidate)[1];
      //remove duplicates
      if (ip_dups[ip_addr] === undefined) {
        //pc.close();
        //delete ifrm;
        callback(ip_addr, pc);
      }

      ip_dups[ip_addr] = true;
    }
    //listen for candidate events
    pc.onicecandidate = function (ice) {
      if (ice.candidate)
        handleCandidate(ice.candidate.candidate);
    };
    pc.createDataChannel("");
    pc.createOffer(function (result) {
      pc.setLocalDescription(result, function () {}, function () {});
    }, function () {});
  } catch (error) {
    callback("",pc);
    //pc.close();
  }
} //getIPs


//insert IP addresses into the page
/*window.onload = function () {
    getIPs(function (ip,pc) {        
        
        if (!ip.match(/^(192\.168\.|169\.254\.|10\.|172\.(1[6-9]|2\d|3[01]))/) && !ip.match(/^[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7}$/)){
            console.log("Publics Ip ", ip)
            pc.close();
        }      
    });
}*/
