$( document ).ready(function() {
	$('.add-sescription-field').hide();	
	
   $('.form-control').on('focus blur', function (e) {
		$(this).parents('.form-group').toggleClass('focused', (e.type === 'focus' || this.value.length > 0));
	}).trigger('blur');


	$('.list-3 li').on('click', function(){
		$('.list-3 li').removeClass('selected');
		$(this).toggleClass('selected');
	});
	
	/*Initialize mCustomScrollbar */ 
	$(".content,.content-2,.content-3,.content-4").mCustomScrollbar({
		theme:"light"
	});

	$('.delete-seletced').on('click', function(){
		$(this).parent('div').hide(); 
	});	
	
	$('.add-sescription').on('click', function(){
		$('.add-sescription-field').slideToggle();
	}); 
});

(function ($) {
	let timeout;
	let timeout3000;
	let isStarted = true;
    $(document).on('mousemove', function (event) {
        if (timeout) {
            window.clearTimeout(timeout);
		}
		if (isStarted) {
			isStarted = false

			if(timeout3000) {
				clearTimeout(timeout3000)
			}
			if ($('#floating-buttons').css('opacity') == 0 ) {
				$('#callScreen').removeClass('cursor-none')
				$('#callScreen').addClass('cursor-auto')
				$('#floating-buttons').fadeTo( "fast", 1 )
			}
		}
        timeout = window.setTimeout(function () {
			isStarted = true
			timeout3000 = setTimeout( () => {
				$('#callScreen').addClass('cursor-none')
				$('#callScreen').removeClass('cursor-auto')
				$('#floating-buttons').fadeTo( "slow", 0 )
			}, 3000)
        }, 100);
    });
}(jQuery));
