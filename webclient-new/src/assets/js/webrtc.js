var WebrtcObj = null;
var front = false;
console.log(adapter.browserDetails);
function Webrtc(io) {
  var debug = new Debug(true);
  var self = this;
  getIceConfig = () => {
    let iceServers = [];
    if (this.iceServers.length) {
      for (let index = 0; index < this.iceServers.length; index++) {
        let element = this.iceServers[index];
        if (index === 0) {
          iceServers.push(getSTUNObj(element.url));
        } else {
          iceServers.push(
            getTURNObj(element.url, element.username, element.credential)
          );
        }
      }
    } else {
      iceServers.push(getSTUNObj("stun:stun.l.google.com:19302"));
      iceServers.push(
        getTURNObj("turn:webrtcweb.com:7788", "muazkh", "muazkh")
      );
      iceServers.push(
        getTURNObj("turn:webrtcweb.com:8877", "muazkh", "muazkh")
      );
      iceServers.push(
        getTURNObj("turns:webrtcweb.com:7788", "muazkh", "muazkh")
      );
    }
    return { iceServers: iceServers };
  };

  var isJoin = false,
    firststreamid = "";
  const _env = adapter.browserDetails;

  this.DataChannel = false;
  this.video = false;
  this.peers = {};
  this.roomid = "webrtc";
  this.socketUrl = "";
  this.iceServers = [];
  this.audioBandwidth = 240;
  this.videoBandwidth = 480;
  this.devices = { audio: [], video: [] };
  this.customUniqueId = "" + new Date().getTime() + "";

  this.constraints = { audio: false, video: false };
  this.localstreams = {};
  this.remotestreams = {};
  this.queryParams = ''

  //default server syn values
  this.data = {};
  this.data.userId = Date.now().toString();
  this.data._env = _env;
  this.data.extra = {};
  this.data.firststreamid = firststreamid;
  this.data.streams = [];
  this.data.streamStatus = {};
  (this.pluginUrl = ""), (this.isPlugin = false);

  //remote users
  this.remoteusers = {};

  this.socket = null;

  var audioBandwidth_new = 240;
  var videoBandwidth_new = 480;

  
  //----------------------------------------//
  function getPlatformName() {
    var isMobile = {
      Android: function() {
        return navigator.userAgent.match(/Android/i);
      },
      BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
      },
      iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
      },
      Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
      },
      Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
      },
      any: function() {
        return (
          isMobile.Android() ||
          isMobile.BlackBerry() ||
          isMobile.iOS() ||
          isMobile.Opera() ||
          isMobile.Windows()
        );
      }
    };
    return isMobile;
  } //getPlatformName
  function setCameraFacingMode(constraints) {
    //only for mobile devices.
    if (getPlatformName().Android()) {
      front = !front;
      constraints.video = {
        //for android
        facingMode: front ? "user" : "environment"
      };
    } else if (getPlatformName().iOS()) {
      //for ios
      front = !front;
      constraints.video = {
        //facingMode:'front'//front,back
        facingMode: front ? "user" : "environment"
      };
    }
    return constraints;
  } //setFacingMode

  connectSocket = callback => {
    var callback = callback || function() {};
    if (self.socket) {
      callback(self.socket);
      return;
    }
    this.constraints = setCameraFacingMode(this.constraints);
    //var param = self.data;

    //param = JSON.stringify(param);
    //self.socket = io.connect(this.socketUrl, { query: "param="+param });
    self.socket = io.connect(this.socketUrl, {
      query: "customUniqueId=" + this.customUniqueId
    }); //{ query: 'param1=value1&param2=value2'}
    console.log("this.socketUrl", this.socketUrl);
    //self.socket = io.connect(this.socketUrl);

    self.socket.on("connect", () => {
      debug.log("connected", self.socket.id);
      return false;
    });

    self.socket.on("connect_error", e => {
      debug.error("connect_error", e);
    });

    self.socket.on("peer.connected", params => {
      self.remoteusers[params.id] = params.data;
      self.peers[params.id] = self.peers[params.id] || {};
      self.peers[params.id].isOfferer = true;
      self.data.streams.forEach(streamid => {
        self.peerAddStream(self.localstreams[streamid], params.id);
        makeOffer(streamid, params.id);
      });
      self.onGetRemotes();
    });

    self.socket.on("stream.connected", params => {
      self.remoteusers[params.id] = params.data;
      get_PC(params.id, params.streamid);
      self.onGetRemotes();
    });

    self.socket.on("stream.removed", params => {
      let customUniqueId = params.id;
      let streamid = params.streamid;
      let data = params.data;

      self.remoteusers[customUniqueId] = data;

      debug.info("remote stream removed");
      delete self.remotestreams[customUniqueId].streams[streamid];
      close_PC(customUniqueId, streamid);

      self.onRemoteStreamended({
        id: customUniqueId,
        streamid: streamid,
        type: "remote",
        data: data
      });
      self.onGetRemotes();
    });
    self.socket.on("peer.disconnected", params => {
      debug.log("peer.disconnected", params.id);
      delete self.remoteusers[params.id];
      delete self.remotestreams[params.id];
      close_PC(params.id);
      self.onGetRemotes();
      self.onpeerdisconnect(params);
    });
    self.socket.on("signalling", data => {
      handleMessage(data);
    });
    self.socket.on("switching", params => {
      self.remoteusers[params.id] = params.data;
      handleSwitching(params);
      self.onGetRemotes();
      self.onSwitching(params);
    });
    self.socket.on("updatedata", params => {
      self.remoteusers[params.id] = params.data;
      self.onGetRemotes();
      self.onupdatedata(params.data);
    });
    self.socket.on("notifications", data => {
      self.onNotification(data);
    });

    self.socket.on("typing", data => {
      self.onTyping(data);
    });
    callback(self.socket);
  };
  //connectSocket start
  self.connectSocket = callback => {
    connectSocket(callback);
  };
  //connectSocket end

  this.sendNotification = data => {
    if (this.socket) {
      this.socket.emit("notifications", data);
    }
  };

  this.userTyping = data => {
    if (this.socket) {
      this.socket.emit("typing", data);
    }
  };
  this.closeRoom = () => {
    isJoin = false;
    if (this.socket) {
      this.socket.close();

      for (var key in self.localstreams) {
        if (this.localstreams.hasOwnProperty(key)) {
          this.localstreams[key].getTracks().forEach(track => {
            this.localstreams[key].removeTrack(track);
            if (track.stop) {
              track.stop();
            }
          });
          //this.oncloseroom({ id: currentSocketId, data: self.data});
        }
      }
      this.data.firststreamid = "";
      this.data.streams = [];
      this.data.streamStatus = {};
      this.remoteusers = {};
      this.socket = null;
      self.onGetRemotes(self.remoteusers);
    }
    close_PC();

    /*for (let socketId in self.remoteusers) {
            if(self.remoteusers.hasOwnProperty(socketId)){
                self.remoteusers[socketId].streams.forEach(streamid=>{
                    var pc = get_PC(socketId, streamid);
                    pc.close();
                })
            }
        }*/
  };
  this.updatedata = callback => {
    debug.info("update data");
    this.socket.emit("updatedata", self.data);
    if (callback) {
      callback(self.data);
    }
  };

  this.isExistRoom = (roomId, callback) => {
    connectSocket(socket => {
      socket.emit("isExist", roomId, callback);
    });
  };

  this.getRemoteUsers = id => {
    if (id) {
      return self.remoteusers[id] || null;
    } else {
      return self.remoteusers;
    }
  };
  this.countRemoteUsers = () => {
    return Object.size(this.remoteusers);
  };

  //navigator.getUserMedia start
  getMedia = (constraints, callback) => {
    navigator.mediaDevices
      .getUserMedia(constraints)
      .then(stream => {
        callback(null, stream);
      })
      .catch(e => {
        callback(e, null);
      });
  };

  this.joinroom = callback => {
    if (isJoin) {
      callback("already Join Offered", null);
      return false;
    }
    this.getDevices(dv => {
      fireDevice(dv);

      this.constraints = getConstraints();
      if (!this.constraints.audio) {
        return callback("no audio devices found", null);
      }
      if (!this.constraints.video && this.video) {
        return callback("no video devices found", null);
      }
      connectSocket(socket => {
        getMedia( { audio: true, video: this.video }, (error, stream) => {
          if (error) {
            return callback(error, null);
          }
          //for getting proper device name  in firefox
          this.getDevices(fireDevice);

          isJoin = true;
          stream.type = "local";
          self.data.firststreamid = firststreamid = stream.id;
          self.data.streams.push(firststreamid);
          self.localstreams[firststreamid] = stream;
          this.setStreamStatus(firststreamid);
          self.socket.emit("join", self.roomid, self.data, rooms => {
            delete rooms[this.customUniqueId];
            for (let customUniqueId in rooms) {
              self.peers[customUniqueId] = self.peers[customUniqueId] || {};
              self.peers[customUniqueId].isOfferer = false;
            }
            self.remoteusers = rooms;
            self.peerAddStream(self.localstreams[firststreamid]);
            self.onGetRemotes();
          });
          callback(null);
          self.onlocalstream({
            id: this.customUniqueId,
            stream: stream,
            data: self.data
          });
        });
      });
    });
  };
  getConstraints = () => {
    this.constraints.audio = !!this.devices.audio.length;
    this.constraints.video = !!this.devices.video.length;
    return this.constraints;

    // return constraints = {
    //         audio: true,

    //         video: {
    //             mandatory: {
    //                 maxWidth: 640
    //             },
    //             optional: [
    //                        {maxFrameRate: 15},
    //                        {minFrameRate: 15}
    //                        ]
    //         }

    //     };
  };
  replaceStream = (oldstream, newstream) => {
    let newAudioTracks = newstream.getAudioTracks();
    let newVideoTracks = newstream.getVideoTracks();

    replaceTrack = track => {
      for (let customUniqueId in self.remoteusers) {
        if (self.remoteusers.hasOwnProperty(customUniqueId)) {
          var pc = get_PC(customUniqueId, oldstream.id);
          let senders = pc.getSenders();
          senders.map(sender => {
            if (sender.track.kind === track.kind) {
              sender.replaceTrack(track);
            }
          });
        }
      }
    };

    if (newAudioTracks.length) {
      oldstream.getAudioTracks().forEach(track => {
        oldstream.removeTrack(track);
      });
      newAudioTracks.forEach(track => {
        oldstream.addTrack(track);
        replaceTrack(track);
      });
    }

    if (newVideoTracks.length) {
      oldstream.getVideoTracks().forEach(track => {
        oldstream.removeTrack(track);
      });
      newVideoTracks.forEach(track => {
        oldstream.addTrack(track);
        replaceTrack(track);
      });
    }
    // if (_env.browser == "chrome") makeOffer(oldstream.id);
  };
  switchStreamByConstraints = (oldstreamid, constraints, callback) => {
    debug.info("switch stream start");
    if (oldstreamid != firststreamid) {
      var error = "only main stream can be switched ";
      callback(error);
      return;
    }
    var records = {
      id: this.customUniqueId,
      streamid: oldstreamid,
      data: self.data,
      status: "start"
    };
    self.socket.emit("switching", records);
    try {
      if (self.isStream(oldstreamid) && self.canAddStream()) {
        let oldstream = self.localstreams[firststreamid];

        if (
          _env.browser == "firefox" ||
          getPlatformName().Android() ||
          getPlatformName().iOS()
        ) {
          if (constraints.audio) {
            oldstream.getAudioTracks().forEach(track => track.stop());
          }
          if (constraints.video) {
            oldstream.getVideoTracks().forEach(track => track.stop());
          }
        }

        getMedia(constraints, (error, newstream) => {
          if (error) {
            records.status = "fail";
            self.socket.emit("switching", records);
            console.log("getMedia", error, constraints);
            callback(error);
            return;
          }
          replaceStream(self.localstreams[firststreamid], newstream);
          reloadVideoTag(self.localstreams[firststreamid]);
          records.status = "success";
          self.socket.emit("switching", records);
          callback(null, self.localstreams[firststreamid]);
        });
      }
    } catch (e) {
      records.status = "fail";
      self.socket.emit("switching", records);
      callback(e);
    }
  };

  getDeviceConstraints = constraints => {
    if (typeof constraints === "object") {
      if (constraints.audio !== undefined || constraints.video !== undefined) {
        let newCons = {};
        if (constraints.audio) {
          newCons.audio = { deviceId: { exact: constraints.audio } };
        } else {
          newCons.audio = false;
        }

        if (constraints.video) {
          newCons.video = { deviceId: { exact: constraints.video } };
        } else {
          newCons.video = false;
        }
        return newCons;
      }
    } else {
      return false;
    }
  };

  this.switchDevice = (device, callback) => {
    let e = "Devices are busy.";

    strReplaceDefault = str => str.replace("default: ", "");

    let usedLabel = { audio: "", video: "" };
    let newLabel = { audio: "", video: "" };

    let stream = this.localstreams[firststreamid];
    stream.getAudioTracks().forEach(track => {
      usedLabel.audio = strReplaceDefault(track.label);
    });

    stream.getVideoTracks().forEach(track => {
      usedLabel.video = strReplaceDefault(track.label);
    });

    if (device.audio) {
      this.devices.audio.forEach(machine => {
        if (machine.value == device.audio) {
          newLabel.audio = strReplaceDefault(machine.label);
        }
      });
    }

    if (device.video) {
      this.devices.video.forEach(machine => {
        if (machine.value == device.video) {
          newLabel.video = strReplaceDefault(machine.label);
        }
      });
    }

    if (usedLabel.audio == newLabel.audio) {
      delete device.audio;
    }
    if (usedLabel.video == newLabel.video) {
      delete device.video;
    }

    let constraints = getDeviceConstraints(device);
    if (device.video) {
      constraints = setCameraFacingMode(constraints);
    }
    if (constraints) {
      switchStreamByConstraints(firststreamid, constraints, callback);
    } else {
      callback(e);
    }
  };

  //navigator.getUserMedia end
  this.isStream = streamid => {
    if (self.localstreams[streamid] === undefined) {
      debug.error("no local stream found with this streamid: " + streamid);
      return false;
    } else {
      return true;
    }
  };
  this.canAddStream = () => {
    if (!Object.size(self.remoteusers)) {
      debug.error("cannot add stream because no remote users found");
      return false;
    } else {
      return true;
    }
  };
  this.canRemoveStream = streamid => {
    if (firststreamid === streamid) {
      debug.error("cannot remove first stream: " + streamid);
      return false;
    } else {
      return true;
    }
  };

  this.peerAddStream = (stream, customUniqueId) => {
    var add = (cId, streamid) => {
      var pc = get_PC(cId, streamid);
      //pc.addStream(stream);
      stream.getTracks().forEach(track => pc.addTrack(track, stream));
    };
    if (customUniqueId) {
      add(customUniqueId, stream.id);
    } else {
      for (let cId in this.remoteusers) {
        if (this.remoteusers.hasOwnProperty(cId)) {
          add(cId, stream.id);
        }
      }
    }
  };
  this.peerAddTrack = (track, stream, customUniqueId) => {
    stream.addTrack(track);
    var add = (cId, streamid) => {
      var pc = get_PC(cId, streamid);
      pc.addTrack(track, stream);
    };
    if (customUniqueId) {
      add(customUniqueId, stream.id);
    } else {
      for (let cId in this.remoteusers) {
        if (this.remoteusers.hasOwnProperty(cId)) {
          add(cId, stream.id);
        }
      }
    }
  };
  this.peerRemoveStream = (stream, customUniqueId) => {
    var streamid = stream.id;
    var remove = (customUniqueId, streamid) => {
      var pc = get_PC(customUniqueId, streamid);
      if (_env.browser == "firefox") {
        pc.getSenders().forEach(sender => {
          stream.getTracks().forEach(track => {
            if (sender.track === track) {
              pc.removeTrack(sender);
            }
          });
        });
      } else {
        pc.removeStream(stream);
      }
      stream.getTracks().forEach(track => {
        track.stop();
        stream.removeTrack(track);
      });
      if (streamid !== firststreamid) {
        setTimeout(() => {
          close_PC(customUniqueId, streamid);
        }, 1000);
      }
    };

    if (customUniqueId) {
      remove(customUniqueId, stream.id);
    } else {
      for (let customUniqueId in this.remoteusers) {
        if (this.remoteusers.hasOwnProperty(customUniqueId)) {
          remove(customUniqueId, stream.id);
        }
      }
    }
  };
  String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, "g"), replacement);
  };
  setBandwidth = sdp => {
    // var temp = sdp.sdp;
    // temp = temp.replace(/b=AS:.*\r\n/, "").replace(/b=TIAS:.*\r\n/, "");
    // temp = temp.replace(/b=AS:.*\r\n/, "").replace(/b=TIAS:.*\r\n/, "");
    // temp = temp.replace(
    //   /a=mid:audio\r\n/gi,
    //   "a=mid:audio\r\nb=AS:" + self.audioBandwidth + "\r\n"
    // );
    // temp = temp.replace(
    //   /a=mid:video\r\n/gi,
    //   "a=mid:video\r\nb=AS:" + self.videoBandwidth + "\r\n"
    // );
    // sdp.sdp = temp;
    // return sdp;

    if (!this.queryParams) {
      return sdp
    }
    
    const queryParams = queryParamToOpusString(this.queryParams)
    let temp = sdp.sdp;
    temp = temp.replace('useinbandfec=1', `useinbandfec=1; ${queryParams}`);
    // temp = temp.replace('useinbandfec=1', `useinbandfec=1; stereo=1; maxaveragebitrate=${this.opusAudioBitRate*8}`);

    sdp.sdp = temp;
    return sdp;
  };

  //---------------------------------------------------//
  this.updateBandwidth = updateBandwidth = sdp => {
    var temp = sdp.sdp;
    temp = temp.replace(/b=AS:.*\r\n/, "").replace(/b=TIAS:.*\r\n/, "");
    temp = temp.replace(/b=AS:.*\r\n/, "").replace(/b=TIAS:.*\r\n/, "");
    temp = temp.replace(
      /a=mid:audio\r\n/gi,
      "a=mid:audio\r\nb=AS:" + audioBandwidth_new + "\r\n"
    );
    temp = temp.replace(
      /a=mid:video\r\n/gi,
      "a=mid:video\r\nb=AS:" + videoBandwidth_new + "\r\n"
    );
    sdp.sdp = temp;
    return sdp;
  };
  //---------------------------------------------------//
  self.removeStream = streamid => {
    if (self.isStream(streamid) && this.canRemoveStream(streamid)) {
      removeStream(streamid);
    }
  };
  removeStream = streamid => {
    var stream = self.localstreams[streamid];
    delete self.localstreams[streamid];
    delete self.data.streamStatus[streamid];
    //this.peerRemoveStream(stream);
    //makeOffer(streamid);

    var index = self.data.streams.indexOf(streamid);
    self.data.streams.splice(index, 1);
    stream.getTracks().forEach(track => {
      track.stop();
      stream.removeTrack(track);
    });
    self.onLocalStreamended({
      id: this.customUniqueId,
      streamid: stream.id,
      type: "local",
      data: self.data
    });
    self.socket.emit("removeStream", streamid, self.data);

    close_PC(null, streamid);
  };

  this.sendTextMessage = msg => {
    var obj = {};
    obj.data = this.data;
    obj.type = "text";
    obj.message = msg;
    for (let customUniqueId in this.remoteusers) {
      if (this.remoteusers.hasOwnProperty(customUniqueId)) {
        self.peers[customUniqueId].channel.send(JSON.stringify(obj));
      }
    }
  };

  //------------------------------------------------//
  this.getMyPeer = (socketId, callback) => {
    if (self.peers[socketId].firststreamid) {
      callback(self.peers[socketId].firststreamid.pc);
    } else {
      callback(undefined);
    }
  };
  this.sendFileMessage = msg => {
    if (msg.type) {
      var obj = {};
      obj.data = this.data;
      obj.type = "file";
      obj.message = msg;
      for (let socketId in this.remoteusers) {
        if (this.remoteusers.hasOwnProperty(socketId)) {
          self.peers[socketId].channel.send(JSON.stringify(obj));
        }
      }
    } else {
      for (let socketId in this.remoteusers) {
        if (this.remoteusers.hasOwnProperty(socketId)) {
          self.peers[socketId].channel.send(msg);
        }
      }
    }
  };

  self.changeBandWidth = (streamid, quality) => {
    let audioBandwidth;
    let videoBandwidth;
    if (quality == "high") {
      audioBandwidth = 240;
      videoBandwidth = 1000;
    } else if (quality == "medium") {
      audioBandwidth = 240;
      videoBandwidth = 800;
    } else if (quality == "low") {
      audioBandwidth = 240;
      videoBandwidth = 250;
    }
    audioBandwidth_new = audioBandwidth;
    videoBandwidth_new = videoBandwidth;
    
    if ((_env.browser === 'chrome' ||
        (_env.browser === 'firefox' &&
        _env.version >= 64)) &&
        'RTCRtpSender' in window &&
        'setParameters' in window.RTCRtpSender.prototype) {
          for (let customUniqueId in self.remoteusers) {
            if (self.remoteusers.hasOwnProperty(customUniqueId)) {
              var pc = get_PC(customUniqueId, firststreamid);
              const sender = pc.getSenders()[1];
              const parameters = sender.getParameters();
              if (!parameters.encodings) {
                parameters.encodings = [{}];
              }
              parameters.encodings[0].maxBitrate = videoBandwidth_new * 1000;
              sender.setParameters(parameters)
              .then(() => {
              })
              .catch(e => console.error(e));
            }
          }
    }else{
      makeOffer(streamid);
    }

    // makeOffer(streamid);
    // setResolution(streamid , quality , (success , err) => {
    //     if(success){
    //         makeOffer(streamid);
    //     }else{
    //         console.log(err);
    //     }
    // });
  };

  this.sendWhiteBoard = data => {
    for (let socketId in this.remoteusers) {
      if (this.remoteusers.hasOwnProperty(socketId)) {
        self.peers[socketId].channel.send(data);
      }
    }
  };
  
  // setResolution = (streamid, quality , callback) => {
  //     if (!this.localstreams[streamid]) {
  //         let errMsg = 'undefined streamid';
  //         callback(null , errMsg);
  //     }else{
  //         var videoConstraints = {aspectRatio: 1.777777778};
  //         if(quality == 'high'){
  //             videoConstraints.width = 1024;
  //             videoConstraints.height = 768;
  //             videoConstraints.frameRate = 30;
  //         }else if(quality == 'medium'){
  //             videoConstraints.width = 960;
  //             videoConstraints.height = 540;
  //             videoConstraints.frameRate = 30;
  //         }else if(quality == 'low'){
  //             videoConstraints.width = 640;
  //             videoConstraints.height = 480;
  //             videoConstraints.frameRate = 15;
  //         }
  //         let stream = this.localstreams[streamid];
  //         let video = stream.getVideoTracks();
  //         if (video.length) {
  //             let videoConst =  video[0].enabled === true ? videoConstraints : false;
  //             this.data.streamStatus[stream.id].video = videoConst;
  //             callback('success' , null);
  //         }else{
  //             let errMsg = 'unable to enable video because no video track found';
  //             callback(null , errMsg);
  //         }
  //     }
  // }
  //------------------------------------------------//

  close_PC = (customUniqueId, streamid) => {
    let closePeer = (sId, strId) => {
      if (
        this.peers &&
        this.peers[sId] &&
        this.peers[sId][strId] &&
        this.peers[sId][strId].pc
      ) {
        let pc = this.peers[sId][strId].pc;
        pc.close();
        delete this.peers[sId][strId];
      }
    };
    if (!customUniqueId && streamid) {
      for (let sId in this.peers) {
        if (this.peers.hasOwnProperty(sId)) {
          if (
            this.peers[sId].hasOwnProperty(streamid) &&
            streamid != "isOfferer"
          ) {
            closePeer(sId, streamid);
          }
        }
      }
    } else if (customUniqueId && streamid) {
      closePeer(customUniqueId, streamid);
    } else if (customUniqueId) {
      for (let streamid in this.peers[customUniqueId]) {
        if (
          this.peers[customUniqueId].hasOwnProperty(streamid) &&
          streamid != "isOfferer"
        ) {
          closePeer(customUniqueId, streamid);
        }
      }
    } else {
      for (let customUniqueId in this.peers) {
        if (this.peers.hasOwnProperty(customUniqueId)) {
          for (let streamid in this.peers[customUniqueId]) {
            if (
              this.peers[customUniqueId].hasOwnProperty(streamid) &&
              streamid != "isOfferer"
            ) {
              closePeer(customUniqueId, streamid);
            }
          }
        }
      }
    }
  };

  get_PC = (customUniqueId, streamid) => {
    let backupStreamId = streamid;
    streamid = streamid === firststreamid ? "firststreamid" : streamid;
    self.peers[customUniqueId][streamid] =
      self.peers[customUniqueId][streamid] || {};
    if (self.peers[customUniqueId][streamid].pc) {
      return self.peers[customUniqueId][streamid].pc;
    }
    let type = "remote";
    let isScreen = streamid == "firststreamid" ? false : true;
    if (self.localstreams[streamid]) {
      type = "local";
    }
    console.log("streamid", streamid);

    let tempData = {
      socketId: customUniqueId,
      streamid: streamid,
      type: type,
      isScreen: isScreen
    };

    var pc = new RTCPeerConnection(getIceConfig());
    self.peers[customUniqueId].renegotiated = false;

    self.peers[customUniqueId][streamid].pc = pc;
    let interval = null;
    if (_env.browser == "chrome") {
      let oldData;
      interval = setInterval(() => {
        pc.getStats(data => {
          getStatsObj(_env.browser, data, result => {
            if (!oldData) {
              result.bandwidth_persecond = result.bandwidth.speed;
              result.byteSent_persecond = result.totalBytesSend;
              result.bytesReceived_persecond = result.totalBytesReceived;

              result.packetsSentAudio_ps = result.packetsSentAudio;
              result.packetsLostSentAudio_ps = result.packetsLostSentAudio;
              result.packetsRecAudio_ps = result.packetsRecAudio;
              result.packetsLostRecAudio_ps = result.packetsLostRecAudio;

              result.bytesSentAudio_ps = result.audio.bytesSent;
              result.bytesReceivedAudio_ps = result.audio.bytesReceived;

              result.packetsSentVideo_ps = result.packetsSentVideo;
              result.packetsLostSentVideo_ps = result.packetsLostSentVideo;
              result.packetsRecVideo_ps = result.packetsRecVideo;
              result.packetsLostRecVideo_ps = result.packetsLostRecVideo;

              result.bytesSentVideo_ps = result.video.bytesSent;
              result.bytesReceivedVideo_ps = result.video.bytesReceived;
            } else {
              result.bandwidth_persecond =
                result.bandwidth.speed - oldData.bandwidth.speed;
              result.byteSent_persecond =
                result.totalBytesSend - oldData.totalBytesSend;
              result.bytesReceived_persecond =
                result.totalBytesReceived - oldData.totalBytesReceived;

              result.packetsSentAudio_ps =
                result.packetsSentAudio - oldData.packetsSentAudio;
              result.packetsLostSentAudio_ps =
                result.packetsLostSentAudio - oldData.packetsLostSentAudio;
              result.packetsRecAudio_ps =
                result.packetsRecAudio - oldData.packetsRecAudio;
              result.packetsLostRecAudio_ps =
                result.packetsLostRecAudio - oldData.packetsLostRecAudio;

              result.bytesSentAudio_ps =
                result.audio.bytesSent - oldData.audio.bytesSent;
              result.bytesReceivedAudio_ps =
                result.audio.bytesReceived - oldData.audio.bytesReceived;

              result.packetsSentVideo_ps =
                result.packetsSentVideo - oldData.packetsSentVideo;
              result.packetsLostSentVideo_ps =
                result.packetsLostSentVideo - oldData.packetsLostSentVideo;
              result.packetsRecVideo_ps =
                result.packetsRecVideo - oldData.packetsRecVideo;
              result.packetsLostRecVideo_ps =
                result.packetsLostRecVideo - oldData.packetsLostRecVideo;

              result.bytesSentVideo_ps =
                result.video.bytesSent - oldData.video.bytesSent;
              result.bytesReceivedVideo_ps =
                result.video.bytesReceived - oldData.video.bytesReceived;
            }
            oldData = result;
            self.onStatistics(tempData, result);
          });
        });
      }, 1000);
    } else {
      let oldData;
      interval = setInterval(() => {
        // event.streams[0].getTracks()[0]
        pc.getStats(null).then(function(data) {
          getStatsObj(_env.browser, data, result => {
            // console.log("tempData moz" , tempData);
            result.packetsLostSentAudio_ps = "NA";
            result.packetsLostSentVideo_ps = "NA";
            if (!oldData) {
              result.bandwidth_persecond = result.bandwidth.speed;
              result.byteSent_persecond = result.totalBytesSend;
              result.bytesReceived_persecond = result.totalBytesReceived;

              result.packetsSentAudio_ps = result.packetsSentAudio;

              // result.packetsLostSentAudio_ps = result.packetsLostSentAudio;

              result.packetsRecAudio_ps = result.packetsRecAudio;
              result.packetsLostRecAudio_ps = result.packetsLostRecAudio;

              result.bytesSentAudio_ps = result.audio.bytesSent;
              result.bytesReceivedAudio_ps = result.audio.bytesReceived;

              result.packetsSentVideo_ps = result.packetsSentVideo;

              // result.packetsLostSentVideo_ps = result.packetsLostSentVideo;

              result.packetsRecVideo_ps = result.packetsRecVideo;
              result.packetsLostRecVideo_ps = result.packetsLostRecVideo;

              result.bytesSentVideo_ps = result.video.bytesSent;
              result.bytesReceivedVideo_ps = result.video.bytesReceived;
            } else {
              result.bandwidth_persecond =
                result.bandwidth.speed - oldData.bandwidth.speed;
              result.byteSent_persecond =
                result.totalBytesSend - oldData.totalBytesSend;
              result.bytesReceived_persecond =
                result.totalBytesReceived - oldData.totalBytesReceived;

              result.packetsSentAudio_ps =
                result.packetsSentAudio - oldData.packetsSentAudio;

              // result.packetsLostSentAudio_ps = result.packetsLostSentAudio - oldData.packetsLostSentAudio;

              result.packetsRecAudio_ps =
                result.packetsRecAudio - oldData.packetsRecAudio;
              result.packetsLostRecAudio_ps =
                result.packetsLostRecAudio - oldData.packetsLostRecAudio;

              result.bytesSentAudio_ps =
                result.audio.bytesSent - oldData.audio.bytesSent;
              result.bytesReceivedAudio_ps =
                result.audio.bytesReceived - oldData.audio.bytesReceived;

              result.packetsSentVideo_ps =
                result.packetsSentVideo - oldData.packetsSentVideo;

              // result.packetsLostSentVideo_ps = result.packetsLostSentVideo - oldData.packetsLostSentVideo;

              result.packetsRecVideo_ps =
                result.packetsRecVideo - oldData.packetsRecVideo;
              result.packetsLostRecVideo_ps =
                result.packetsLostRecVideo - oldData.packetsLostRecVideo;

              result.bytesSentVideo_ps =
                result.video.bytesSent - oldData.video.bytesSent;
              result.bytesReceivedVideo_ps =
                result.video.bytesReceived - oldData.video.bytesReceived;
            }
            oldData = result;
            self.onStatistics(tempData, result);
          });
        });
      }, 1000);
    }

    if (self.DataChannel && streamid === "firststreamid") {
      setChannelEvents = channel => {
        // force ArrayBuffer in Firefox; which uses "Blob" by default.
        channel.binaryType = "arraybuffer";

        /****************************************************************/
        
        var receivedTotalLength = 0;
        var receivedFileName = "";
        var receiveProgressLength = 0;
        var chunkArray = [];
        var msgData = {};
        var fileObjUrl = null;
        //------------------------------------//

        channel.onmessage = event => {
          if (event.data.byteLength) {
            chunkArray.push(event.data);
            receiveProgressLength =
              receiveProgressLength + event.data.byteLength;
            fileObjUrl = URL.createObjectURL(new window.Blob(chunkArray));
            let obj = {
              receiveProgressLength: receiveProgressLength,
              receivedTotalLength: receiveProgressLength,
              chunkArray: chunkArray,
              receivedTotalLength: receivedTotalLength,
              receivedFileName: receivedFileName,
              data:
                receiveProgressLength == receivedTotalLength ? msgData : null,
              message: fileObjUrl,
              type: "file"
            };
            self.onmessageFile(obj);
            if (receiveProgressLength == receivedTotalLength) {
              receivedTotalLength = 0;
              receivedFileName = "";
              receiveProgressLength = 0;
              // URL.revokeObjectURL(fileObjUrl);
              chunkArray = [];
              msgData = {};
            }
          } else {
            let obj = JSON.parse(event.data);
            if (obj.type === "text") {
              self.onmessage(obj);
            } else if (obj.type === "file") {
              receivedTotalLength = obj.message.totalLength;
              receivedFileName = obj.message.filename;
              msgData = obj.data;
            } else if (obj.type === "whiteboard") {
              self.onmessageWhiteBoard(obj);
            }
          }
        };

        channel.onopen = event => {
          debug.info("channel is opened");
        };

        channel.onerror = error => {
          debug.error(error);
        };

        channel.onclose = event => {
          debug.log("channel is closeed");
        };

        channel.internalSend = channel.send;
        channel.send = data => {
          if (channel.readyState !== "open") {
            return;
          }
          channel.internalSend(data);
        };
      };

      if (!self.peers[customUniqueId].isOfferer) {
        pc.ondatachannel = event => {
          var channel = event.channel;
          setChannelEvents(channel);
          self.peers[customUniqueId].channel = channel;
        };
      } else {
        var channel = pc.createDataChannel("sctp", {});
        setChannelEvents(channel);
        self.peers[customUniqueId].channel = channel;
      }
    }

    pc.onicecandidate = evnt => {
      debug.log("sending ice candidate");
      self.socket.emit("signalling", {
        by: this.customUniqueId,
        to: customUniqueId,
        streamid: streamid,
        ice: evnt.candidate,
        type: "ice"
      });
    };

    let rStreams = {};

    pc.ontrack = event => {
      let stream = event.streams[0];
      if (rStreams[stream.id] === undefined) {
        rStreams[stream.id] = stream;
        debug.log("Received new stream");
        stream.type = "remote";
        var data = {
          id: customUniqueId,
          stream: stream,
          data: self.remoteusers[customUniqueId]
        };

        self.remotestreams[customUniqueId] =
          self.remotestreams[customUniqueId] || {};
        self.remotestreams[customUniqueId].streams =
          self.remotestreams[customUniqueId].streams || {};
        self.remotestreams[customUniqueId].streams[stream.id] = stream;
        self.onremotestream(data);
      }
    };

    //switch start
    pc.onnegotiationneeded = () => {
      //debug.log("onnegotiationneeded",streamid);
      //stop to run it on starting add video in chrome
      // if( !self.peers[socketId].renegotiated && streamid === 'firststreamid') {
      //     self.peers[socketId].renegotiated = true;
      //     return;
      // }
      // self.peers[socketId].renegotiated = true;
      //makeOffer(streamid, socketId);
    };
    pc.onremovestream = event => {
      debug.info("remote stream removed");
      delete self.remotestreams[customUniqueId].streams[event.stream.id];
      if (streamid !== "firststreamid") {
        close_PC(customUniqueId, streamid);
      }
      self.onRemoteStreamended({
        id: customUniqueId,
        streamid: event.stream.id,
        type: "remote",
        data: self.remoteusers[customUniqueId]
      });
    };
    pc.oniceconnectionstatechange = () => {
      let state = pc.iceConnectionState;
      debug.log("iceConnectionState: ", state);
      if (state == "closed") {
        if (interval != null) {
          //console.log('rohit', tempData);
          clearInterval(interval);
          self.stopStates(tempData);
        }
      }
      if (state == "failed") {
        var offerOptions = {
          offerToReceiveAudio: 1, //(audioInput.checked) ? 1 : 0,
          offerToReceiveVideo: 1, //(videoInput.checked) ? 1 : 0,
          iceRestart: true
        };

        pc.createOffer(offerOptions)
          .then(function(sdp) {
            pc.setLocalDescription(sdp);
            debug.log(
              "Creating an offer for customUniqueId:",
              customUniqueId,
              "streamid",
              streamid
            );
            self.socket.emit("signalling", {
              by: self.customUniqueId,
              to: customUniqueId,
              streamid: streamid,
              sdp: setBandwidth(sdp),
              type: "sdp-offer"
            });
          })
          .catch(function(e) {
            debug.error("pc.createOffer error", e);
          });
      }
    };
    //switch end

    return pc;
  };

  makeOffer = (streamid, customUniqueId) => {
    if (!streamid) {
      debug.error("invalid offer");
      return;
    }

    var offer = (streamid, customUniqueId) => {
      if (streamid === firststreamid) {
        streamid = "firststreamid";
      }
      var pc = get_PC(customUniqueId, streamid);
      var offerOptions = {
        // New spec states offerToReceiveAudio/Video are of type long (due to
        // having to tell how many "m" lines to generate).
        // http://w3c.github.io/webrtc-pc/#idl-def-RTCOfferAnswerOptions.
        offerToReceiveAudio: 1, //(audioInput.checked) ? 1 : 0,
        offerToReceiveVideo: 1, //(videoInput.checked) ? 1 : 0,
        iceRestart: false
        //iceRestart: restartInput.checked,
        //voiceActivityDetection: vadInput.checked
      };
      pc.createOffer(offerOptions)
        .then(sdp => {
          pc.setLocalDescription(sdp);
          debug.log(
            "Creating an offer for customUniqueId:",
            customUniqueId,
            "streamid",
            streamid
          );
          self.socket.emit("signalling", {
            by: this.customUniqueId,
            to: customUniqueId,
            streamid: streamid,
            sdp: setBandwidth(sdp),
            type: "sdp-offer"
          });
        })
        .catch(e => {
          debug.error("pc.createOffer error", e);
        });

      /* pc.createOffer((sdp) => {
                pc.setLocalDescription(sdp);
                debug.log('Creating an offer for customUniqueId:', customUniqueId, 'streamid', streamid);
                self.socket.emit('signalling', { by: this.customUniqueId, to: customUniqueId, streamid: streamid, sdp: setBandwidth(sdp), type: 'sdp-offer' });

            }, (e) => { debug.error('pc.createOffer error', e); }
                //,{ mandatory: { offerToReceiveVideo: true, offerToReceiveAudio: true }}
            ); */
    };
    if (customUniqueId) {
      offer(streamid, customUniqueId);
    } else {
      for (let customUniqueId in self.remoteusers) {
        if (self.remoteusers.hasOwnProperty(customUniqueId)) {
          offer(streamid, customUniqueId);
        }
      }
    }
  };

  handleMessage = data => {
    var pc = get_PC(data.by, data.streamid);
    switch (data.type) {
      case "sdp-offer":
        pc.setRemoteDescription(
          new RTCSessionDescription(data.sdp),
          () => {
            debug.log("Setting remote description by offer");
            pc.createAnswer(
              sdp => {
                pc.setLocalDescription(sdp);
                self.socket.emit("signalling", {
                  by: this.customUniqueId,
                  to: data.by,
                  streamid: data.streamid,
                  sdp: setBandwidth(sdp),
                  type: "sdp-answer"
                });
              },
              e => {
                debug.error("createOffer error: ", e);
              }
            );
          },
          e => {
            debug.error("remoteDescription error: ", e);
          }
        );
        break;
      case "sdp-answer":
        let newsdp = updateBandwidth(data.sdp);
        pc.setRemoteDescription(
          new RTCSessionDescription(data.sdp),
          () => {
            debug.log("Setting remote description by answer");
          },
          e => {
            debug.error("remoteDescription error: ", e);
          }
        );
        break;
      case "ice":
        if (data.ice) {
          debug.log("Adding ice candidates");
          pc.addIceCandidate(new RTCIceCandidate(data.ice));
        }
        break;
    }
  };
  handleSwitching = records => {
    switch (records.status) {
      case "start":
        break;
      case "success":
        setTimeout(() => {
          reloadVideoTag(
            self.remotestreams[records.id].streams[records.streamid]
          );
        }, 1000);
        break;
      case "fail":
        debug.error(
          JSON.stringify({
            customUniqueId: records.id,
            streamid: records.streamid,
            switching: "fail"
          })
        );
        break;
    }
  };
  reloadVideoTag = stream => {
    if (!stream) {
      debug.warn("stream not exists to reload");
      return false;
    }
    try {
      document.getElementById(stream.id).srcObject = stream;
    } catch (e) {
      debug.error("no video element id match with streamid:", stream.id);
    }
  };
  self.getDevices = callback => {
    var dv = { audioIp: {}, videoIp: {} };
    if (!navigator.mediaDevices || !navigator.mediaDevices.enumerateDevices) {
      debug.warn("enumerateDevices() not supported.");
      return false;
    }

    // List cameras and microphones.
    navigator.mediaDevices
      .enumerateDevices()
      .then(devices => {
        devices.forEach(device => {
          // if( device.kind == 'default' || device.label == 'Default' || device.deviceId == 'default') {
          //     return false;
          // }
          // if(device.label != 'Default' && device.deviceId != 'default') {

          if (device.kind == "audioinput") {
            dv.audioIp[device.deviceId] = {
              label: device.label,
              value: device.deviceId
            };
          }
          if (device.kind == "videoinput") {
            dv.videoIp[device.deviceId] = {
              label: device.label,
              value: device.deviceId
            };
          }
          // }
        });
        callback(dv);
      })
      .catch(err => {
        let errM = err.name + ": " + err.message;
        debug.log(errM);
        callback(null, errM);
      });
  };

  fireDevice = dv => {
    let devices = { audio: [], video: [] };
    for (let key in dv.audioIp) {
      if (dv.audioIp.hasOwnProperty(key)) {
        devices.audio.push(dv.audioIp[key]);
      }
    }
    for (let key in dv.videoIp) {
      if (dv.videoIp.hasOwnProperty(key)) {
        devices.video.push(dv.videoIp[key]);
      }
    }
    console.log("getDevices");
    this.devices = devices;
    this.ondevice(devices);
  };

  //screen share plugin code
  if (_env.browser == "chrome") {
    //self.pluginUrl = "https://chrome.google.com/webstore/detail/talkroom-screen-sharing/bacdhkkjdbghahbhopkgfijligodechn";
    self.pluginUrl =
      "https://chrome.google.com/webstore/detail/biztalk-screen-sharing/djohcgcekepibiafpaaffmldhimiggmb";
  } else if (_env.browser == "firefox") {
    self.isPlugin = true;
  } else {
    debug.error("screen sharing not supported in your browser");
    self.isPlugin = false;
  }
  this.getPreScreen = callback => {
    let data = { isSupport: true, isPlugin: false, url: "" };
    if (_env.browser == "chrome" && _env.version < 73) {
      var extensionImg = document.createElement("img");
      //extensionImg.setAttribute("src",  "chrome-extension://bacdhkkjdbghahbhopkgfijligodechn/icon.png");
      extensionImg.setAttribute(
        "src",
        "chrome-extension://djohcgcekepibiafpaaffmldhimiggmb/icon.png"
      );
      extensionImg.addEventListener(
        "load",
        e => {
          data.isPlugin = self.isPlugin = true;
          callback(data);
        },
        false
      );
      extensionImg.addEventListener(
        "error",
        e => {
          data.isPlugin = self.isPlugin = false;
          data.url = self.pluginUrl;
          callback(data);
        },
        false
      );
    } else if (_env.browser == "firefox") {
      data.isPlugin = self.isPlugin = true;
      callback(data);
    } else {
      data.isSupport = false;
      callback(data);
    }
  };
  getScreen = callback => {
    if (
      (_env.browser == "chrome" && _env.version >= 72) ||
      (_env.browser == "firefox" && _env.version >= 66)
    ) {
      try {
        navigator.mediaDevices
          .getDisplayMedia({ video: true })
          .then(stream => {
            stream.oninactive = event => {
              this.removeStream(event.target.id);
            };
            return callback(null, stream);
          })
          .catch(err => {
            return callback(err, null);
          });
      } catch (error) {
        return callback(error, null);
      }
    } else {
      return callback({
        message: "NOT_SUPPORTED",
        _env
      });
    }
  };

  this.addExtraStream = stream => {
    if (this.canAddStream()) {
      attachExtraStream(stream);
    }
  };

  attachExtraStream = stream => {
    self.data.streams.push(stream.id);
    self.localstreams[stream.id] = stream;
    stream.type = "local";
    this.setStreamStatus(stream.id);
    //self.onlocalstream({ stream: stream, data: self.data });
    self.onlocalstream({ stream: stream, data: self.data, id: this.customUniqueId });
    self.socket.emit("joinStream", stream.id, self.data, rooms => {
      delete rooms[this.customUniqueId];
      self.remoteusers = rooms;
      self.peerAddStream(stream);
      makeOffer(stream.id);
      self.onGetRemotes();
    });
  };
  this.setStreamStatus = (streamid, status, callback) => {
    if (!callback) {
      callback = () => {}
    }
    if (!this.localstreams[streamid]) {
      console.error("undefined streamid");
      return callback("undefined streamid");
    }
    let stream = this.localstreams[streamid];
    let data = {
      audio: false,
      video: false
    };

    let audio = stream.getAudioTracks();
    let video = stream.getVideoTracks();

    if (!audio.length && status && status.audio) {
      console.warn("unable to enable sound because no audio track found");
    }
    if (audio.length) {
      data.audio = audio[0].enabled;

      if (status && status.audio !== undefined) {
        if (status.audio) {
          if (!audio[0].enabled) {
            data.audio = audio[0].enabled = true;
          }
        } else {
          if (audio[0].enabled) {
            data.audio = audio[0].enabled = false;
          }
        }
      }
    }

    if (video.length) {
      data.video = video[0].enabled;

      if (status && status.video !== undefined) {
        if (status.video) {
          if (!video[0].enabled) {
            data.video = video[0].enabled = true;
          }
        } else {
          if (video[0].enabled) {
            data.video = video[0].enabled = false;
          }
        }
      }
    } else {
      if (!this.video && this.devices.video.length && status && status.video) {
        return navigator.mediaDevices.getUserMedia({ video: true})
        .then(vStream => {
          data.video = true
          this.peerAddTrack (vStream.getVideoTracks()[0], stream )
          makeOffer(stream.id)
          this.data.streamStatus[stream.id] = data;
          callback(null)
        }).catch(e => {
          data.video = false
          this.data.streamStatus[stream.id] = data;
          console.error(e);
          callback('Permission denied Error: It looks like camera is busy')
        });
      }
    }
    this.data.streamStatus[stream.id] = data;
    callback(null)
  };

  this.shareScreen = callback => {
    debug.log("screen share start");
    if (!this.canAddStream()) {
      callback("no remote user");
    } else {
      getScreen((error, stream) => {
        if (error) {
          return callback(error);
        }
        if (stream) {
          self.addExtraStream(stream);
          return callback(null, stream);
        }
      });
    }
  };

  this.setAudioBandwidthForAll = (bandwidth) => {
    // this.opusAudioBitRate = bandwidth
    
    // self.data.streams.forEach(streamid => {
    //   makeOffer(streamid, params.id);
    // });
  }

  //method which are required to overide
  self.ondevice = devices => {
    debug.warn("unused ondevice");
  };
  self.onmessage = data => {
    debug.warn("unused onmessage");
  };
  self.onremotestream = event => {
    debug.warn("unused onremotestream");
  };
  self.onlocalstream = event => {
    debug.warn("unused onlocalstream");
  };
  self.onpeerdisconnect = data => {
    debug.warn("unused onpeerdisconnect");
  };
  self.onLocalStreamended = data => {
    debug.warn("unused onLocalStreamended");
  };
  self.onRemoteStreamended = data => {
    debug.warn("unused onRemoteStreamended");
  };
  this.onupdatedata = params => {
    debug.warn("unused onupdatedata", params);
  };
  this.onGetRemotes = () => {
    debug.warn("unused onGetRemotes");
  };
  //-----------------------------------------------//
  this.onSwitching = data => {
    debug.warn("unused onSwitching");
  };
  this.onNotification = data => {
    debug.warn("unused onNotification");
  };
  this.onTyping = data => {
    debug.warn("unused ontyping");
  };

  this.onStatistics = (tmpData, data) => {
    console.log("unused onStatistics", data);
  };
  this.stopStates = streamId => {};
  this.onmessageWhiteBoard = data => {
    debug.warn("unused onmessageWhiteBoard", data);
  };
  //-----------------------------------------------//
}

Object.size = function(obj) {
  var size = 0,
    key;
  for (var key in obj) {
    if (obj.hasOwnProperty(key)) size++;
  }
  return size;
};
Array.prototype.swap = function(x, y) {
  var b = this[x];
  this[x] = this[y];
  this[y] = b;
  return this;
};
function getSTUNObj(stunStr) {
  var urlsParam = "urls";
  var obj = {};
  obj[urlsParam] = stunStr;
  return obj;
}

function getTURNObj(turnStr, username, credential) {
  var urlsParam = "urls";
  var obj = {
    username: username,
    credential: credential
  };
  obj[urlsParam] = turnStr;
  return obj;
}
function Debug(gState) {
  var debug = {};

  if (gState) {
    for (var m in console) {
      if (typeof console[m] == "function") {
        debug[m] = console[m].bind(window.console[m]);
      }
    }
  } else {
    for (var m in console) {
      if (typeof console[m] == "function") {
        debug[m] = function() {};
      }
    }

    // var logger = document.getElementById('logger');
    // for (var m in console) {
    //     if (typeof console[m] == 'function') {
    //         debug[m] = function () {
    //             var str = ''
    //             for (var i = 1; i < arguments.length; i++) {
    //                 if (typeof arguments[i] == 'object')
    //                     str += JSON && JSON.stringify ? JSON.stringify(arguments[i]) : arguments[i]+' '
    //                 else
    //                     str+=arguments[i]+' '
    //             }
    //             logger.innerHTML += str + '';
    //         }
    //     }
    // }
  }
  return debug;
}
function openwebrtc(io) {
  WebrtcObj = new Webrtc(io);
  return WebrtcObj;
}

function queryParamToOpusString(queryParams) {
  const queryParamsObj = JSON.parse('{"' + decodeURI(queryParams).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g,'":"') + '"}')
  const maxaveragebitrate = queryParamsObj.maxaveragebitrate
  if (maxaveragebitrate) {
    queryParamsObj.maxaveragebitrate = parseFloat(maxaveragebitrate) * 8 * 1000
  }
  let str  = ''
  for (let key in queryParamsObj) {
    if ( queryParamsObj.hasOwnProperty( key ) ) {
      str = `${str}${str ? '; ' : '' }${key}=${queryParamsObj[key]}`
    }
  }
  return str
}
