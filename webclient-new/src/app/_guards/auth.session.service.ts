import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable()

export class AuthSessionService {
    // key: string = 'rtcsession';
    tokenKey: string = 'rtctoken';

    constructor(private router: Router) {}
    isEmpty(obj) {
        return JSON.stringify(obj) === JSON.stringify({});
    }
    // put( content: Object ){
    //     sessionStorage.setItem( this.key , JSON.stringify(content));
    // }
    // delete(){
    //     sessionStorage.removeItem(this.key);
    // }
    // get(){
    //     if(sessionStorage.getItem(this.key)) {
    //         return JSON.parse( sessionStorage.getItem( this.key ) );
    //     } else {
    //         return {};
    //     }
    // }

    // putItem(key: string, item:any) {
    //     let session = this.get();
    //     session[key] = item;
    //     this.put(session);
    // }
    // deleteItem(key: string) {
    //     let session = this.get();
    //     delete session[key];
    //     this.put(session);
    // }
    // getItem(key: string){
    //     let session = this.get();
    //     return session[key] || null;
    // }
    putToken(token) {
        sessionStorage.setItem( this.tokenKey , token);
    }
    getToken() {
        if (sessionStorage.getItem(this.tokenKey)) {
            return sessionStorage.getItem( this.tokenKey );
        } else {
            return '';
        }
    }
    deleteToken() {
        sessionStorage.removeItem(this.tokenKey);
    }

    getMsgs(key: string) {
        const msgs = {
                        signupSuccess: 'Signup Successfully',
                        emailExist: 'Email already Exists',
                        loginSuccess: 'logged in successfully',
                        invalidPassword: 'Wrong password',
                        invalidEmail: 'Invalid Email',
                        invalidAccess: 'Invalid Access',
                        invalidToken: 'Invalid Token',
                        logoutSuccess: 'logout Successfully',
                        norecords: 'no records found',
                    };
        return msgs[key];
    }
    // redirectToDashboard() {
    //     let rid = this.getUserRoles();
    //     let userDashboard = '';
    //     if (this.router.url == '/sign-up') {
    //         userDashboard = 'sign-up-success';
    //     } else {
    //         userDashboard = 'my-learning';
    //     }
    //     return userDashboard;
    // }
}
