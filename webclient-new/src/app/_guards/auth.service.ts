import { Injectable, Inject } from '@angular/core';
import { AuthSessionService } from './auth.session.service';

@Injectable()

export class AuthService {

    Session: AuthSessionService;
    constructor(@Inject(AuthSessionService) session: AuthSessionService) {
        this.Session = session;
    }
    isLoggedIn() {
        if (this.Session.getToken()) {
            return true;
        }
        return false;
    }
}
