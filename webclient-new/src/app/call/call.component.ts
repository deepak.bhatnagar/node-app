import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, FormArray, FormControl } from '@angular/forms';
import { Validators } from '../shared/validator';
import { UrlService } from '../shared/app.url.service';
import { CallService } from '../shared/call.service';
import { UtilityService } from '../shared/utility.service';
import { My } from '../app.data';
import { DomSanitizer } from '@angular/platform-browser';
import { emojis } from '../shared/emoji';
import { WhiteboardComponent } from '../whiteboard/whiteboard.component';
import { Config } from '../app.config';

declare var $;
declare function systemDetail(window): any;
@Component({
    selector: 'app-chat',
    templateUrl: './call.component.html',
    styleUrls: ['./call.component.css']
})
export class CallComponent implements OnInit, OnDestroy {
    buildVersion = Config.buildVersion;
    sessionUser: any;
    errorMsg = '';
    sucessMsg = '';
    errorPopupMsg = '';
    devices: { audio: Array<any>, video: Array<any> } = { audio: [], video: [] };

    // room data
    screenId: string = '';
    streams = [];
    countRemote: number = 0;
    selected = { audio: '', video: '' };
    firstStreamid = '';
    streamStatus = { audio: true, video: false };
    callWindow = 'no'; // "no" || "min" || "max";
    runningCall: any = {};
    roomId = '';
    inviteUrl: string = '';
    copyButtonText: string = 'Copy Url';
    chatWindowStatus = false;
    sendMessages: FormGroup;
    messages = [];
    isUserTyping: boolean = false;
    typingUsers: any = {};
    typingUserList: string = '';
    speakerId: string = '';
    year = new Date().getFullYear();
    screenShareModal = {
        title: '',
        description: ''
    };

    isChatWidow = false
    unreadMessageCount = 0

    videoQuality: string = '';
    platform = systemDetail(window);
    constructor(
        private urlService: UrlService,
        private utils: UtilityService,
        private router: Router,
        private route: ActivatedRoute,
        public callService: CallService,
        private formBuilder: FormBuilder,
        private changeDetect: ChangeDetectorRef,
        private sanitizer: DomSanitizer
    ) { }
    msgStrForm: FormGroup;
    UserNameControlForm: FormGroup;
    modalusername: string;
    // notification : any = {isNotify : false , notifyStr : '' , type : ''};
    notificationData  = { type : '', displayName : '' , notificationMsg : '' , userId : ''};
    isNotify = false;
    timeOut: any;
    subs: any = {};
    localStream = {};
    roomName = '';
    navigateTo: string;
    file: any;
    reader = new FileReader();

    sendProgrss = { max: 0, value: 0 };
    receiveProgrss = { max: 0, value: 0 };
    statistics: any = {};
    statisticsArr = [];
    activeStates: any = {};

    emojiList = [];
    curPosition: number;
    msgStrng = '';
    showWhiteboardParent = false;

    isGalleryView = false;
    // ---------------- //
    ngOnDestroy() {
        for ( let keys in this.subs) {
            if (this.subs.hasOwnProperty(keys) && this.subs[keys]) {
                this.subs[keys].unsubscribe();
            }
        }
        this.closeRoom();
    }
    resetIsNotify() {
        this.timeOut = setTimeout(() => {
            // this.notification.isNotify = false;
            this.isNotify = false;
        }, 5000);
    }

    ngOnInit() {
        this.emojiList = emojis.list;
        // console.log("this.emojiList",  this.emojiList);
        $('#screen-data').click(function() {
            $('.screen-data').addClass('open');
        });
        $('#close-screen-data').click(function() {
            $('.screen-data').removeClass('open');
        });

        this.UserNameControlForm = this.formBuilder.group({
            username: [
                '', Validators.compose([Validators.required, Validators.maxLength(50), Validators.validateAlphabetWtihSpace_n_Dash])
            ],
        });

        this.afterCallStart()
        this.callService.getDevices( (err, devices) => {
            if (err) {
                this.navigateTo = '/';
                this.errorPopupMsg = err;
                $('#errorPopup').modal('show');
                return
            }
            if (devices && devices.videoinput && devices.videoinput.length) {                
                $('#VideoCallConfirmModal').modal('show');
            } else {
                this.openRoom()
            }
        })
        // this.afterCallStart()
        
    }
    afterCallStart () { 
        this.sessionUser = My.getSessionUser();
        this.streamStatus.video = My.getVideoCall()
        if(!this.sessionUser){
            this.sessionUser = {
                isGuest: true, userId: 'guestUserId' + this.getrandam(),
                displayName: 'Guest-' + this.getrandam(),
                profilePath: '/assets/images/default_user.png'
            };
            console.log(this.sessionUser.displayName);
            My.setSessionUser( this.sessionUser );
        }
        
        this.roomName = this.route.snapshot.params['roomId'];
        if (this.roomName) {
            this.roomId = this.roomName;
        }
        if (this.route.snapshot.params['queryParams'] && window.location.search) {
            My.setQueryParams(window.location.search.substring(1))
        }
        // -------------------------------- //
        this.msgStrForm = this.formBuilder.group({ msgStr : '' });
        $('body').removeClass('before-login');

        this.subs.onlocalstream = this.callService.onlocalstream().subscribe( data => {
            data.userData = this.sessionUser;
            this.streams.unshift(data);
            if ( !this.firstStreamid ) {
                this.firstStreamid = data.id;
                this.streamStatus = this.callService.getStreamStatus(this.firstStreamid);

            }
            console.log('onlocalstream', data);
            $('#main-loader').hide();
            
            let notifydata = {
                type  :  'new connection',
                displayName  :  this.sessionUser.displayName,
                notificationMsg  : this.sessionUser.displayName + ' has joined this call',
                userId : this.sessionUser.userId
            };
            this.callService.sendNotification(notifydata);
            this.localStream = data;
            // ------------------------------------------------------ //
        });

        this.subs.localstreamEnded = this.callService.localstreamEnded().subscribe( data => {
            let streamid = data.streamid;
            let i = this.utils.findIndexbyKey(streamid, this.streams, 'id');
            console.log(streamid, this.screenId)
            if (i > -1) {  this.streams.splice(i, 1); }
            if ( streamid === this.screenId ) {
                this.screenId = '';
            }
        });

        this.subs.onremotestream = this.callService.onremotestream().subscribe( data => {
            console.log('onremotestream', data);
            let streamid = data.id;
            let i = this.utils.findIndexbyKey(streamid, this.streams, 'id');
            if (i > -1) {
                this.streams[i].data = data.data;
            } else {
                this.streams.unshift(data);
            }
            // this.callService.getCurrentSpeaker(data);

            //// share screen notification
            //     if(data.data.streams.length == 2){
            //         // console.log(data.data.streams.indexOf(data.data.firststreamid));
            //         if(data.data.streams.indexOf(data.data.firststreamid) != -1){
            //             this.popNotification(data , data , 'share-screen');
            //         }
            //     }
            // //---------------------------------//
            // this.autoplay(data.id);
            if(this.platform.browser === 'Safari'){
                this.changeBandWidth('medium');
            }
             
        });
        this.subs.remotestreamEnded = this.callService.remotestreamEnded().subscribe( data => {
            let streamid = data.streamid;
            let i = this.utils.findIndexbyKey(streamid, this.streams, 'id');
            if (i > -1) { this.streams.splice(i, 1); }
            this.updateStreams(data);
        });

        this.subs.onPeerDisconnect = this.callService.onPeerDisconnect().subscribe( data => {
            this.onPeerDisconnect(data);

            // close room when no remote
            // if(!this.countRemote) {
            //     this.closeRoom();
            // }
        });

        this.subs.onGetRemotes = this.callService.onGetRemotes().subscribe( () => {
            this.countRemote = this.callService.countRemoteUsers();
            console.log('countRemote' , this.countRemote);
            let data = this.callService.getRemoteUsers();
            console.log( 'onGetRemotes', data );
        });

        // ------------------------------------------------------------------------------//
        this.subs.onStatistics = this.callService.onStatistics().subscribe((data) => {
            // let tempData = { socketId: data.socketId, streamid: data.streamid };
            // console.log('rohit start', JSON.stringify(tempData));
            // console.log("statData",data);
            this.statistics[data.type] = this.statistics[data.type] || {};
            this.statistics[data.type][data.socketId] = this.statistics[data.type][data.socketId] || {};
            this.statistics[data.type][data.socketId][data.isScreen] = data;
            this.filterstats();

            // this.utils.objToArray(this.statistics , (data) => {
            //     this.statisticsArr = data;
            //     //console.log(this.statisticsArr);
            //     this.refresh();
            // });
        });
        this.subs.stopStates = this.callService.stopStates().subscribe((data) => {
            // console.log('rohit stop', JSON.stringify(data));
            delete this.statistics[data.type][data.socketId][data.isScreen];
            if (this.utils.isEmpty(this.statistics[data.type][data.socketId])) {
                delete this.statistics[data.type][data.socketId];
            }
            if (this.utils.isEmpty(this.statistics[data.type])) {
                delete this.statistics[data.type];
            }
            if (this.activeStates.type == data.type && this.activeStates.socketId == data.socketId && this.activeStates.isScreen == data.isScreen) {
                this.activeStates = {};
            }
        });
        this.subs.onSwitching = this.callService.onSwitching().subscribe( (data) => {
            console.log('onSwitching', data);
        });

        this.subs.onNotification = this.callService.onNotification().subscribe( (data) => {
            console.log('onNotification', data);
            this.notificationData = data;

            // differentiating same user name when share screen
            // if(this.notificationData.type == 'share-screen'){
            //     console.log("this.streams" , this.streams);
            //     let streams = this.streams;
            //     for (let index = 0; index < streams.length; index++) {
            //         if( streams[index].data.userId === this.notificationData.userId){
            //             this.streams[index].data.displayName = this.streams[index].data.displayName+'\'s screen';
            //         }
            //     }
            // }
            // ------------------------------------------------//

            clearTimeout(this.timeOut);
            this.isNotify = true;
            this.resetIsNotify();
            // if(this.notificationData.type == 'new message'){
            //     let isvisible = $('.full-screen-group-right').hasClass("full-screen-visible");
            //     if(!isvisible){
            //         clearTimeout(this.timeOut);
            //         this.isNotify = true;
            //         this.resetIsNotify();
            //     }
            // }else{
            //     clearTimeout(this.timeOut);
            //     this.isNotify = true;
            //     this.resetIsNotify();
            // }
        });

        this.subs.onmessageFile = this.callService.onmessageFile().subscribe((data) => {
            console.log('onmessageFile', data);
            if (this.receiveProgrss.max == 0) {
                this.receiveProgrss.max = data.receivedTotalLength;
                $('#receiveProgress').show();
            }
            this.receiveProgrss.value = data.receiveProgressLength;
            if (data.data != null) {
                this.receiveProgrss.max = 0;
                this.receiveProgrss.value = 0;
                $('#receiveProgress').hide();
                this.pushMessage(data);
            }
            if (!this.isChatWidow) {
                this.unreadMessageCount ++
                setTimeout(() => {
                    $('#floating-buttons').fadeTo( "fast", 1 )
                }, 500)
            }
            this.refresh();

        });

        this.subs.onTyping = this.callService.onTyping().subscribe( (data) => {
            this.notificationData.notificationMsg = data.message;
            // console.log(this.sessionUser.userId , data.userId);
            if (this.sessionUser.userId != data.userId) {
                if (this.typingUsers[data.userId]) {
                    console.log(this.typingUsers[data.userId], 'already exist');
                } else {
                    this.typingUsers[data.userId] = data.displayName;
                    this.updateTypings();
                }
                console.log(this.typingUserList[data.userId]);
            }
            console.log(data.userId);
            setTimeout(() => {
                if (data.userId) {
                    delete this.typingUsers[data.userId];
                }
                this.updateTypings();
            }, 10000);
        });

        this.subs.onSpeak = this.callService.onSpeak().subscribe( data => {
            this.showCurrentSpeaker(data);
        });
        // ------------------------------------------------------------------------------//

        this.subs.ondevice = this.callService.ondevice().subscribe( (devices) => {
            this.devices = devices;
            // if(this.devices.audio && this.devices.audio.length){
            //     this.selected.audio = this.devices.audio[0].value;
            // }
            // if(this.devices.video && this.devices.video.length){
            //     this.selected.video = this.devices.video[0].value;
            // }
            console.log('ondevice', this.devices);
            if (this.devices) {
                this.setDefaultDevice();
            }
        });

        this.subs.onupdatedata = this.callService.onupdatedata().subscribe( (data) => {
            console.log('onupdatedata', data);
            this.updateMessageArray(data.userId, data.displayName);
            this.updateStreams(data);
        });
        this.subs.onmessage = this.callService.onmessage().subscribe( (data) => {
            delete this.typingUsers[data.data.userId];
            this.updateTypings();

            this.pushMessage(data);
            if (!this.isChatWidow) {
                this.unreadMessageCount ++
                setTimeout(() => {
                    $('#floating-buttons').fadeTo( "fast", 1 )
                }, 500)
            }
        });
    }
    // ---------SHOW HIDE STATS---------//
    showStatistics(obj) {
        this.activeStates = obj;
        $('.screen-data').addClass('open');
    }
    hideStatistics() {
        this.activeStates = {};
        $('.screen-data').removeClass('open');
    }
    // ---------------------------------//
    filterstats() {
        var data = this.activeStates;
        // console.log("var data" , data);
        this.statisticsArr = [];

        if (!data ||
            !this.statistics[data.type] ||
            !this.statistics[data.type][data.socketId] ||
            !this.statistics[data.type][data.socketId][data.isScreen]) {
            if (data.type == 'local') {
                let temp1 = this.statistics[data.type];
                // console.log("this.statistics" , this.statistics);
                for (let socketId in temp1) {
                    if (temp1.hasOwnProperty(socketId)) {
                        let temp2 = temp1[socketId];
                        for (let isScreen in temp2) {
                            if (temp2.hasOwnProperty(isScreen)) {
                                let obj = temp2[isScreen];
                                obj.name = data.name;
                                this.statisticsArr.push(obj);
                            }
                        }
                    }
                }
            }
        } else {
            let obj = this.statistics[data.type][data.socketId][data.isScreen];
            obj.name = data.name;
            this.statisticsArr.push(obj);
        }
        this.refresh();
    }
    updateTypings() {
        let arr = [];
        for (let key in this.typingUsers ) {
            arr.push(this.typingUsers[key]);
        }
        this.typingUserList = arr.toString();
    }
    showCurrentSpeaker(data) {
        this.refresh();
        if (data.isUserSpeaking) {
            this.speakerId = data.streamId;
        } else {
            this.speakerId = '';
        }
    }
    changeBandWidth(quality) {
        this.callService.changeBandWidth(this.firstStreamid, quality);
        this.synData();
        this.videoQuality = quality;
    }
    setAudioBandwidthForAll(noBytes) {
        this.callService.setAudioBandwidthForAll(noBytes);
    }
    updateMessageArray(userId , name) {
        this.messages.forEach(element => {
            if (element.from == userId) {
                element.name = name;
            }
        });
    }
    sanitize(url: string) {
        return this.sanitizer.bypassSecurityTrustUrl(url);
    }
    refresh() {
        this.changeDetect.detectChanges();
    }
    openUserName(val) {
        this.modalusername = val;
        $('#UserNameModal').modal('show');
    }
    setuserName(str) {
        if (str == 'ok') {
            this.sessionUser.displayName = this.modalusername.trim();
            this.updateData({ displayName: this.sessionUser.displayName });
        }
    }
    setDefaultDevice() {
        if (!this.utils.isEmpty(this.localStream)) {
            let tmpStream: any = this.localStream;
            console.log(tmpStream);
            var tracks = tmpStream.getTracks();
            for (let idx = 0 ; idx < tracks.length ; idx++) {
                // let id = tracks[idx].getSettings().deviceId;
                let lbl = tracks[idx].label;
                console.log(lbl);
                if (this.devices.audio && this.devices.audio.length) {
                    this.devices.audio.forEach((ele , idx) => {
                        console.log(ele);
                        if (ele.label == lbl) {
                            this.selected.audio = this.devices.audio[idx].value;
                        }
                    });
                }
                if (this.devices.video && this.devices.video.length) {
                    this.devices.video.forEach((ele , idx) => {
                        if (ele.label == lbl) {
                            this.selected.video = this.devices.video[idx].value;
                        }
                    });
                }
            }
        }
    }
    handleInputFile(e) {
        this.file = e.target.files[0];
        this.sendProgrss.max = this.file.size;
        let sizeMB = this.file.size / 1024 / 1024;
        if (sizeMB < 5) {

            $('#sendProgress').show();
            var fileBlob = URL.createObjectURL(new Blob([this.file]));
            var sliceFile = (offset) => {
                var func = (file) => {
                    let obj = { type : 'file', totalLength: file.size, filename: file.name };
                    this.callService.sendFileMessage(obj);
                    return (e) => {
                        console.log(e);
                        this.callService.sendFileMessage(e.target.result);
                        if (this.file.size > offset + e.target.result.byteLength) {
                            window.setTimeout(sliceFile, 0, offset + 16384);
                            this.sendProgrss.value = offset + e.target.result.byteLength;
                        } else {
                            let tempjsonobj: any = {};
                            tempjsonobj = {
                                name: this.sessionUser.displayName,
                                from: this.sessionUser.userId,
                                message: fileBlob,
                                created_at: (new Date),
                                profilePath: this.sessionUser.profilePath,
                                type: 'file',
                                filename: file.name
                            };
                            this.messages.push(tempjsonobj);
                            this.sendProgrss.value = 0;
                            $('#sendProgress').hide();
                            $('#idInpuFile').val('');
                            // URL.revokeObjectURL(fileBlob);
                        }
                    };
                };
                this.reader.onload = func(this.file);
                var slice = this.file.slice(offset, offset + 16384);
                this.reader.readAsArrayBuffer(slice);

            };
            sliceFile(0);
        } else {
            alert("file size is too large");
        }

    }
    // -------------------//
    groupCallShowHide() {
        $('#call-group-user').slideToggle();
        $( '.group-toggle' ).toggleClass( 'active-chat' );
    }

    updateStreams(data) {
        
        for (let index = 0; index < this.streams.length; index++) {
            if (this.streams[index].userId === data.userId) {
                this.streams[index].data = data;
            }
        }
        // if(shouldNotify){
        //     this.popNotification(oldData , data , 'video-audio');
        // }
        console.log('streams', this.streams);
    }
    // --------------------------GETTING TWILIO SERVER DETAIL---------------------------//
    getTwilioIceServer(callback) {
        this.subs.getTwilioIceServer = this.urlService.getTwilioIceServer().subscribe(
            resp => {
                if (resp.result.length) {
                    this.callService.setIceServers(resp.result);
                    callback(null);
                } else {
                   // this.getXirsys(callback);
                   callback('unable to connect');
                }
            },
            error => {
                console.error('getTwilioIceServer', error);
                callback('error');
            }
        );
    }
    // ---------------------------------------------------------------------------------//
    swapArrayElements = function(arr, indexA, indexB) {
        var temp = arr[indexA];
        arr[indexA] = arr[indexB];
        arr[indexB] = temp;
    };

    moveLarge(streamid) {
        if (this.isGalleryView) {
            return false
        }
        let i = this.utils.findIndexbyKey(streamid, this.streams, 'id');
        if (i == -1) { console.error('unexpected'); return false; }
        this.swapArrayElements(this.streams, i, 0);
    }
    videoPlay(streamid) {
        // if($('#'+streamid)[0] != undefined ) {
        //     try {
        //         $('#'+streamid)[0].play();
        //     } catch (error) {
        //         console.warn(error);
        //     }
        // }
        let ss = document.getElementById(streamid);
        if (ss != undefined ) {
            $(ss)[0].play();
        }
    }
    // openRoom( data: any, callback ) {
    //     console.log('openRoom', data);
    //     this.callService.openRoom(data.roomid , (err) => {
    //         if (err) {
    //              this.navigateTo = '/';
    //              this.errorPopupMsg = 'Unable to access your Camera/Microphone.';
    //              $('#errorPopup').modal('show');
    //         } else {
    //             this.runningCall = data;
    //         }
    //         callback(err);
    //     });
    // }
    openRoom() {
        $('#main-loader').show();
        this.getTwilioIceServer(error => {
            if (error) {
                $('#main-loader').hide();
                this.errorPopupMsg = 'error in getting iceServer';
                this.navigateTo = '/';
                $('#errorPopup').modal('show');
                return
            }
            this.callService.openRoom(this.roomId , (err) => {
                if (err) {
                    $('#main-loader').hide();
                    console.error(err);
                    this.errorPopupMsg = 'Unable to access your Camera/Microphone.';
                    this.navigateTo = '/';
                    $('#errorPopup').modal('show');
                    
                } else {
                    // this.inviteUrl = document.location.origin + '/#/call/' + this.roomName;
                    this.inviteUrl = document.location.origin + '/call/' + this.roomName;
                    // this.runningCall = data;
                }
            });
        });
    }
    shareScreen() {
        let isShared = false;
        this.streams.forEach((e) => {
            if (e.data.streams.length > 1) {
                isShared = true;
            }
        });
        if (!this.screenId) {
            if (!isShared) {
                $('#main-loader').show();
                this.callService.shareScreen((error, stream) => {
                    $('#main-loader').hide();
                    if (error) {
                        this.screenShareModal.title = 'Screen sharing error';
                        if (error.message == 'NOT_SUPPORTED') {
                            if (error._env.browser == 'chrome' || error._env.browser == 'firefox') {
                                this.screenShareModal.description = `Your browser doesn't meet the minimun requirement for screen sharing. Please upgrade your browser.`;
                            } else {
                                this.screenShareModal.description = `Screen sharing is not supported in your browser.`;
                            }
                            $('#screenErrorModal').modal('show');
                        } else {
                            this.screenShareModal.description = 'Something went wrong. Please try again later or refresh your browser';
                        }
                        console.error('error', error);
                    }
                    this.screenId = stream.id;
                    // ---------START PREVENT SHARE SCREEN STREAM FROM MIRRORING--------- //

                    if (this.streams && this.streams.length) {

                        for (let i = 0; i <= this.streams.length; i++) {
                            if (this.streams[i].type == 'remote') {
                                this.moveLarge(this.streams[i].id);
                                break;
                            }
                        }
                    }
                    // ---------END PREVENT SHARE SCREEN STREAM FROM MIRRORING--------- //

                    const data = {
                        type: 'share-screen',
                        displayName: this.sessionUser.displayName,
                        notificationMsg: this.sessionUser.displayName + ' has shared screen',
                        userId: this.sessionUser.userId
                    };
                    this.callService.sendNotification(data);
                    // -------------------------------------------------//
                });
            } else {
                this.screenShareModal.title = 'Screen sharing warning';
                this.screenShareModal.description = 'Screen is already shared by another person.';
                $('#screenErrorModal').modal('show');
            }
        } else {
            this.callService.removeStream(this.screenId);

            const data = {
                type: 'remove-screen',
                displayName: this.sessionUser.displayName,
                notificationMsg: this.sessionUser.displayName + ' has removed screen',
                userId: this.sessionUser.userId
            };
            this.callService.sendNotification(data);
            // -------------------------------------------------//
        }
    }
    deviceSwitch() {
        console.log('deviceSwitch', this.selected);
        if (this.selected.audio && this.selected.video) {
            this.callService.switchDevice (this.selected, (error, stream) => {
                if (error) {
                    console.log(error);
                    // alert('devices are not supported to switch');
                    this.navigateTo = this.router.url;
                    this.errorPopupMsg = 'device may be busy or not supported to switch.';
                    $('#errorPopup').modal('show');
                    return;
                }
                if (stream) {
                    console.log('succeded');
                    
                    let data = {
                        type  :  'switch-device',
                        displayName  :  this.sessionUser.displayName,
                        notificationMsg  : this.sessionUser.displayName + ' has switched device',
                        userId : this.sessionUser.userId
                    };
                    this.callService.sendNotification(data);
                    // ------------------------------------------------------------//
                    this.setSelectedLabel(stream);
                }
            });
            $('#deviceSwitchModal').modal('hide');
        } else {
            alert('Please select both device.');
        }
    }

    // -------------------------------------------//
    endCall() {
        $('#EndCallConfirmModal').modal('show');
    }
    closeRoom() {
        $('.closeRoom').hide();


        // let data = {
        //     type  :  'disconnect',
        //     displayName  :  this.sessionUser.displayName,
        //     notificationMsg  : this.sessionUser.displayName + ' has disconnected'}
        // this.callService.sendNotification(data);
        // -------------------------------------------------//

        // setTimeout(() => {
        $('.full-screen-group-right').removeClass('full-screen-visible');
        $('.normal-screen').removeClass('after-call-attend-full-screen');
        this.callService.closeRoom();

        // room data
        this.runningCall = {};
        this.screenId = '';
        this.streams = [];
        this.countRemote = 0;
        this.selected = { audio: '', video: '' };
        this.firstStreamid = '';
        this.streamStatus = { audio: true, video: false };
        this.inviteUrl = '';
        this.router.navigate(['/']);
        // }, 1000);
    }
    onPeerDisconnect(data) {
        let i = this.utils.findIndexbyKey(data.id, this.streams, 'socketid');
        let obj = this.streams[i];
        while (i > -1 ) {
            this.streams.splice(i, 1);
            i = this.utils.findIndexbyKey(data.id, this.streams, 'socketid');
        }

        // ================================================= START LOGIC ========================================================//
        
        /*
            ---------- REMOVING LOCAL USER SHARED SCREEN (STREAM) IN CASE OF 0 REMOTE USERS TO AVOID DISTORTING SCREEN ISSUE ----------
        */
        if(this.countRemote === 0 && this.streams.length > 1){
            for (let index = 0; index < this.streams.length; index++) {
                if ( this.streams[index].data.firststreamid !== this.streams[index].id) {
                    this.shareScreen();
                }
            }
        }
        
        // ================================================== END LOGIC ========================================================//
        
        // this.popNotification( obj, obj , 'disconnect');


        console.log('onPeerDisconnect', obj);
        this.notificationData.type = 'disconnect';
        this.notificationData.displayName = obj.data.displayName;
        this.notificationData.notificationMsg = obj.data.displayName + ' has disconnected';
        this.isNotify = true;
        this.resetIsNotify();
        // -----------------------------------------------//
    }
    addRemoveVideo() {
        this.callService.setStreamStatus(this.firstStreamid, { video: !this.streamStatus.video }, (error) => {
            if (error) {
                this.errorPopupMsg = 'Unable to access your Camera/Microphone.';
                $('#errorPopup').modal('show');
                return
            }
            console.log('addRemoveVideo')
            this.streamStatus.video  = !this.streamStatus.video;
        
            let action = this.streamStatus.video == true ? 'video call' : 'audio call';
            let data = {
                type  :  'video',
                displayName  :  this.sessionUser.displayName,
                notificationMsg  : this.sessionUser.displayName + ' has switched to ' +  action,
                userId : this.sessionUser.userId
            };
            this.callService.sendNotification(data);
            // -------------------------------------------------//
            this.synData();
        });
        
    }
    muteUnmute() {
        this.callService.setStreamStatus(this.firstStreamid, { audio: !this.streamStatus.audio }, () => {});
        this.streamStatus.audio = !this.streamStatus.audio;
        
        let action = this.streamStatus.audio == true ? 'played' : 'stopped';
        let data = {
            type  :  'audio',
            displayName  :  this.sessionUser.displayName,
            notificationMsg  : this.sessionUser.displayName + ' has ' +  action + ' audio',
            userId : this.sessionUser.userId
        };
        this.callService.sendNotification(data);
        // -------------------------------------------------//
        this.synData();
    }
    synData() {
        this.callService.synData((data) => {
            this.updateStreams(data);
        });
    }
    updateData(data) {
        this.callService.updateData( data, (data) => {
            this.updateStreams(data);
        });
    }
    copyUrl() {
        let copyArea = document.createElement('input');
        copyArea.value = this.inviteUrl;
        document.body.appendChild(copyArea);
        $(copyArea)[0].select();
        try {
            let successful = document.execCommand('copy');
            if (successful) {
                this.copyButtonText = 'copied';
            } else {
                this.copyButtonText = 'failed to copy';
            }
            console.log('Copying text command was ' + this.copyButtonText);
        } catch (err) {
            this.copyButtonText = 'failed to copy';
            console.log('Oops, unable to copy');
        } finally {
            document.body.removeChild(copyArea);
        }
    }
    getrandam() {
        return ( '' + Math.random()).substring(4, 7);
    }
    showMiniChat() {
        this.isChatWidow = !this.isChatWidow
        this.unreadMessageCount = 0
    }
    sendMessage(event) {
        let obj = $('#sendMessageText'); // $("#sendMessageText").value();
        let text = obj[0].value;
        text = text.trim();
        this.msgStrForm.reset();
        if (text) {
            obj[0].value = '';
            this.callService.sendTextMessage(text);
            let tempjsonobj: any = {};
            tempjsonobj = {
                name: this.sessionUser.displayName,
                from: this.sessionUser.userId,
                message: text,
                created_at: (new Date),
                profilePath: this.sessionUser.profilePath,
                type: 'text',
                filename: ''
            };
            this.messages.push(tempjsonobj);
            this.msgStrng = '';
            
            // let notifydata = {
            //     type  :  'new message',
            //     displayName  :  this.sessionUser.displayName,
            //     notificationMsg  : this.sessionUser.displayName + ' has sent new message'}
            // this.callService.sendNotification(notifydata);
            // //-------------------------------------//
        }
    }
    keyDownFunction(evt) {
        this.sendMessage(evt);
    }

    insertEmoji(str) {
        if (this.msgStrng.length > 0) {
            this.msgStrng = [this.msgStrng.slice(0, this.curPosition), str, this.msgStrng.slice(this.curPosition)].join('');
        } else {
            this.msgStrng = str;
        }
        $('.emlist').slideUp();
        $('#sendMessageText').focus();
        //console.log('this.msgStrng',  this.msgStrng);
    }
    getTextPos(val) {
        this.curPosition = val.selectionStart;
    }
    getTypingUser() {
        let msgStr =  this.msgStrForm.value.msgStr;
        if (msgStr && msgStr.length >= 1) {
            let typingData = {
                type  :  'user typing',
                displayName  :  this.sessionUser.displayName,
                message : this.sessionUser.displayName + ' is typing...',
                userId : this.sessionUser.userId
            };
            this.callService.getTypingUser( typingData );
        }
    }
    pushMessage(obj) {
        let jsonobj = obj;
        let tempjsonobj: any = {};
        tempjsonobj = {
            name: jsonobj.data.displayName,
            from: jsonobj.data.userId,
            message: jsonobj.message,
            created_at: (new Date),
            profilePath: jsonobj.data.profilePath,
            type: jsonobj.type,
            filename: jsonobj.type == 'file' ? jsonobj.receivedFileName : ''
        };
        this.messages.push(tempjsonobj);
        this.refresh();
    }

    setSelectedLabel(stream?) {
        if (!stream) {
            let i = this.utils.findIndexbyKey(this.firstStreamid, this.streams, 'id');
            if (i == -1) {
                return false;
            }
            stream = this.streams[i];
        }
        let selectedLabel = { audio: '', video: '' };

        stream.getAudioTracks().forEach((track) => {
            selectedLabel.audio = track.label;
        });

        stream.getVideoTracks().forEach((track) => {
            selectedLabel.video = track.label;
        });
        for (let index = 0; index < this.devices.audio.length; index++) {
            let element = this.devices.audio[index];
            if (element.label == selectedLabel.audio) {
                this.selected.audio = element.value;
            }
        }
        for (let index = 0; index < this.devices.video.length; index++) {
            let element = this.devices.video[index];
            if (element.label == selectedLabel.video) {
                this.selected.video = element.value;
            }
        }
    }
    openWhiteBoard(sts) {
        this.showWhiteboardParent = sts;
    }
    startVideoCall(status) {
        My.setVideoCall( status );
        this.openRoom()
    }
    getDisplayname(stream) {
        return stream.data.userId == this.sessionUser.userId ?  stream.data.firststreamid == stream.id ? 'you' : 'your screen' : stream.data.firststreamid == stream.id ? stream.data.displayName : stream.data.displayName +'-screen'
    }
    toggleGalary() {
      this.isGalleryView = !this.isGalleryView;
    }
  
}
