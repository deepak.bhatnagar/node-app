import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageNotFoundRouting } from './page-not-found.routing';
import { PageNotFoundComponent } from './page-not-found.component';

@NgModule({
    imports: [
        PageNotFoundRouting,
        CommonModule,
    ],
    declarations: [ PageNotFoundComponent ],
    providers: []
})

export class PageNotFoundModule { }



