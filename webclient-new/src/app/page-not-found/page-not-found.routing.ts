import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes, Router } from '@angular/router';
import { PageNotFoundComponent } from './page-not-found.component';

const appRoutes: Routes = [
   { path: '', component: PageNotFoundComponent }
];

export const PageNotFoundRouting: ModuleWithProviders = RouterModule.forChild( appRoutes );
