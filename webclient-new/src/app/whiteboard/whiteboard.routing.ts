import{ ModuleWithProviders } from '@angular/core';
import{ RouterModule, Routes, Router } from '@angular/router';
import{ WhiteboardComponent } from './whiteboard.component';

const appRoutes: Routes = [
   { path: '', component: WhiteboardComponent }
];

export const WhiteboardRouting: ModuleWithProviders = RouterModule.forChild( appRoutes );
