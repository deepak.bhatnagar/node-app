import { Component, OnInit , AfterViewInit , Input , ElementRef , ViewChild , EventEmitter , OnDestroy } from '@angular/core';
import { CallService } from '../shared/call.service';
declare function openCanvasDesigner(): any;
declare var $;
@Component({
  selector: 'app-whiteboard',
  templateUrl: './whiteboard.component.html',
  styleUrls: ['./whiteboard.component.css']
})

export class WhiteboardComponent implements OnInit,OnDestroy,AfterViewInit {
  @Input() showWhiteboard_child: boolean = false ;
  designer:any;
  subs:any = {};
  CallServ : any;
  emitMousedown_evt: EventEmitter<any> = new EventEmitter<any>();
  mousedown_evt() { return this.emitMousedown_evt }
  
  @ViewChild('widgetcontainer') widgetcontainer: ElementRef;
  constructor(
    private CallService: CallService
   ) {
    this.CallServ = this.CallService;
   }
  
  

  ngOnInit() {
    this.designer = openCanvasDesigner();
    // this.designer.widgetHtmlURL = 'https://cdn.webrtc-experiment.com/Canvas-Designer/widget.html';
    // this.designer.widgetJsURL = 'https://cdn.webrtc-experiment.com/Canvas-Designer/widget.js';
    // this.designer.widgetJsURL = 'assets/js/widget.js'; 
    console.log("this.designer" , this.designer);
    this.designer.setSelected('pencil');

    this.designer.setTools({
        pencil: true,
        text: true,
        image: true,
        eraser: true,
        line: true,
        arrow: true,
        dragSingle: true,
        dragMultiple: true,
        arc: true,
        rectangle: true,
        quadratic: true,
        bezier: true,
        marker: true,
        zoom: true
    });

    this.setDesignerEvent();


    this.subs.onmessageWhiteBoard = this.CallServ.onmessageWhiteBoard().subscribe( data => {
        let isremote = 'isremote';
        data[isremote] = true;
        this.designer.updateWhiteBoard(data);
    });
  }
  ngAfterViewInit(){
    $("#sub-widget-container").load("assets/templates/widget.html");
  }
  ngOnDestroy(){

  }
  
  setDesignerEvent(){
    this.designer.sendWhiteBoard = (data) => {
      this.CallServ.sendWhiteBoard(data);
    } 
  }

  clearCanvas(){
    this.designer.clearCanvas();
  }
  
  

}