import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { WhiteboardRouting } from './whiteboard.routing';
import { WhiteboardComponent } from './whiteboard.component';
import { CallService } from '../shared/call.service';

@NgModule({
  declarations: [
    WhiteboardComponent,
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    WhiteboardRouting,
    CommonModule
  ],
  providers: [ CallService],
})
export class WhiteboardModule {}