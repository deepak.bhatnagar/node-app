import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';
import {Component} from '@angular/core';
export class Config {
  location: Location;
  constructor(location: Location) { this.location = location; }
  // console.log(location.path());
  public static apiEndpoint =  'https://'+location.hostname+':61288/';
  public static chatEndpoint = 'https://'+location.hostname+':61288';
  public static callEndpoint = 'https://'+location.hostname+':61288/call';
}