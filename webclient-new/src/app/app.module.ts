import { BrowserModule } from '@angular/platform-browser';
import { LocationStrategy, PathLocationStrategy} from '@angular/common';

import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { routing } from './app.routing';


import { AppComponent } from './app.component';
import { NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from './shared/shared.module';


import { HomeModule } from './home/home.module';
import { PrivacyModule } from './privacy/privacy.module';


import { UrlService } from './shared/app.url.service';
import { UtilityService } from './shared/utility.service';
import { AuthSessionService } from './_guards/auth.session.service';

@NgModule({
  declarations: [ AppComponent ],
  imports: [
    NgbModule.forRoot(),
    BrowserModule,
    FormsModule,
    SharedModule,
    ReactiveFormsModule,
    HttpModule,
    HomeModule,
    PrivacyModule,
    routing
  ],
  providers: [{ provide: LocationStrategy, useClass: PathLocationStrategy }, UrlService, UtilityService, AuthSessionService],
  bootstrap: [AppComponent]
})
export class AppModule { }
