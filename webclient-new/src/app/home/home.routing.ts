import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';

import { HomeComponent } from './home.component';

const appRoutes: Routes = [
   { path: '', component: HomeComponent}
];

export const HomeRouting: ModuleWithProviders = RouterModule.forChild( appRoutes );
