import 'rxjs/add/operator/finally';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { Validators } from '../shared/validator';
import { UrlService } from '../shared/app.url.service';
import { My } from '../app.data';
declare var $: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {

  constructor(
    public urlService: UrlService,
    private formBuilder: FormBuilder,
    private router: Router
  ) { }
  year = (new Date).getFullYear() ;
  loginForm: FormGroup;
  errorMsg: string = '';
  sessionUser: any;
  ngOnInit() {
    $('body').addClass('before-login');
    this.loginForm = this.formBuilder.group({
      roomname: ['', Validators.compose([Validators.required, Validators.maxLength(50), Validators.validateAlphabetWtihoutSpace])],
      username: ['', Validators.compose([Validators.maxLength(50)])],
    });

    $('.form-control').on('focus blur', function(e) {
    // console.log(e.type,this.value.length);
    $(this).parents('.form-group').toggleClass('focused', (e.type === 'focus' || this.value.length > 0));
    }).trigger('blur');
  }

  getrandam() {
    return ( '' + Math.random()).substring(4, 7);
  }
  validateAlphabetWtihoutSpace(value){
    if(value){
      let EMAIL_REGEXP = new RegExp('^[a-zA-Z0-9._-]+$');
      return EMAIL_REGEXP.test(value) ? true : false;
    } else {
      return true;
    }
  }
  changeErrorMessage(error){
    if (error) {
      if (this.loginForm.get('roomname').hasError('required') && this.loginForm.get('roomname').touched) {
        this.errorMsg = "Room name is required";
      } else if (this.loginForm.get('roomname').hasError('validateAlphabetWtihoutSpace') && this.loginForm.get('roomname').touched) {
        this.errorMsg = "Invalid room name";
      } else if (this.loginForm.get('username').touched && !this.validateAlphabetWtihoutSpace(this.loginForm.value.username)) {
        this.errorMsg = "Invalid user name";
      }
    } else {
      this.errorMsg = "";
    }
    return;
  }
  onSubmit(model) {

    this.sessionUser = {
        isGuest: true,
        userId: 'guestUserId' + this.getrandam(),
        displayName: model.username ? model.username : 'Guest-' + this.getrandam(),
        profilePath: '/assets/images/default_user.png'
    };
    My.setSessionUser( this.sessionUser );
    this.router.navigate(['call/' + model.roomname]);
  }

}
