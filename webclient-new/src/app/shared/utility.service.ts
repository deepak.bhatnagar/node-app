import { Injectable } from '@angular/core';

@Injectable()


export class UtilityService {

    constructor() {};


    isEmpty(obj) {
        return JSON.stringify(obj) === JSON.stringify({});
    }
    
    inArray(val, arr){
        let index = this.findIndex( val, arr );
        return index > -1;
    }

    findIndex(val, arr){
        let index = -1;
        for (let i = 0; i < arr.length; i++) {
            if( val == arr[i] ) {
                index = i;
                break;
            }
        }
        return index;
    }
    findIndexbyKey(val, arr, key) {
        let index = -1;
        for (let i = 0; i < arr.length; i++) {
            if( val == arr[i][key] ) {
                index = i;
                break;
            }
        }
        return index;
    }

    popArray(val, arr) {
        
        let index  = this.findIndex( val, arr );
        if( index > -1) {
            arr.splice(index,1);
        }
        return arr;
    }

    //filters = [{ property:'columnName',direction:-1 }]
    sort(records: Array<any>, fiters: Array<any>): any {
        for (let index = 0; index < fiters.length; index++) {
            var element = fiters[index];

            records.sort(function(a, b){
                if(a[element.property] < b[element.property]){
                    return -1 * element.direction;
                }
                else if( a[element.property] > b[element.property]){
                    return 1 * element.direction;
                }
                else{
                    return 0;
                }
            });
        }
        return records;
    };

    //-----------------------------------------------------------//
    compairObj(a,b){
        let result = false;
        var aProps = Object.getOwnPropertyNames(a);
        var bProps = Object.getOwnPropertyNames(b);
        if (aProps.length != bProps.length) {
            result = true;
        }

        for (var i = 0; i < aProps.length; i++) {
            var propName = aProps[i];
            if (a[propName] !== b[propName]) {
                result = true;
            }
        }
        return result;
    }
    objToArray(obj , callback){
        var newObj = Object.keys(obj);
        var arr = [];
        for (let prop of newObj) { 
            arr.push(obj[prop]);
        }
        callback(arr);
    }
    //-----------------------------------------------------------//
}
