import { Inject, Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { AuthSessionService } from '../_guards/auth.session.service';
import { Config } from '../app.config';
@Injectable()

export class UrlService {

    session: AuthSessionService;
    constructor(
        @Inject(AuthSessionService) session: AuthSessionService,
        private http: Http
        ) {
            this.session = session;
        }
    private baseUrl = Config.apiEndpoint;
    private webServiceUrl = {

        userSignup: this.baseUrl + 'user/signup',
        userLogin: this.baseUrl + 'user/login',
        userlist: this.baseUrl + 'user/listing',
        userlogincheck: this.baseUrl + 'user/isLogin',
        userlogout: this.baseUrl + 'user/logout',
        usergroups: this.baseUrl + 'user/groups',
        groupCreate: this.baseUrl + 'group/create',
        groupUpdate: this.baseUrl + 'group/update',
        groupDetails: this.baseUrl + 'group/details',
        groupUsersDetails: this.baseUrl + 'group/userslisting',
        groupUsersAdd: this.baseUrl + 'group/useradd',
        upload: this.baseUrl + 'upload',
        userUpdate: this.baseUrl + 'user/update',
        afterLogin: this.baseUrl + 'user/afterlogin',
        contactUs: this.baseUrl + 'contactUs',
        forgotPassword: this.baseUrl + 'forgotPassword',
        changePassword: this.baseUrl + 'user/changePassword',
        changeUsername: this.baseUrl + 'user/changeUsername',
    };

    get(name: string): string {
        console.log('apiEndpoint', this.webServiceUrl[name]);
        return this.webServiceUrl[name];
    }

    postData( postUrl: string , body?: Object): Observable<any> {

        let msgBody = { reqParam: body ? body : '' };
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('x-access-token', `${this.session.getToken()}`);
        // headers.append('x-access-token', `${'rohityadav'}`);
        let options = new RequestOptions( { headers: headers } );

        return this.http.post( this.get( postUrl ) , msgBody, options) // ...using post request
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(JSON.stringify(error) || 'Server error')); // ...errors if any
    }

    uploadFile(file: File): Observable<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Content-Type', 'multipart/form-data');
        headers.append('x-access-token', `${this.session.getToken()}`);

        let formData:FormData = new FormData();
        formData.append('uploadFile', file, file.name);
        console.log(file, formData);
        // headers.append('x-access-token', `${'rohityadav'}`);
        let options = new RequestOptions( { headers: headers } );

        return this.http.post( this.webServiceUrl.upload , formData, options) // ...using post request
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(JSON.stringify(error) || 'Server error')); // ...errors if any
    }

    fileChange(event) {
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            let file: File = fileList[0];
            let formData: FormData = new FormData();
            formData.append('uploadFile', file, file.name);
            let headers = new Headers();
            /** No need to include Content-Type in Angular 4 */
            // headers.append('Content-Type', 'multipart/form-data');
            headers.append('Accept', 'application/json');


            console.log(file, formData);
            let options = new RequestOptions({ headers: headers });
            this.http.post(this.webServiceUrl.upload , formData, options)
                .map(res => res.json())
                .catch(error => Observable.throw(error))
                .subscribe(
                    data => { console.log('success'); },
                    error => { console.log(error); }
                );
        }
    }

    getTwilioIceServer(): Observable<any> {
        let msgBody = {};
        let headers = new Headers();
        // headers.append('Authorization', "Basic " + btoa("AC153f2213d8a52c53533b99bac954afea:a10958dbf71ba7be6b1c79609386e432")); Anil account
        headers.append('Authorization', 'Basic ' + btoa('AC1548e5e14ab9ef52f9570050c0a29e8d:b7aadedc939d36839ddc51392bb469b3'));
        headers.append('Content-Type', 'application/json');
        // let options = new RequestOptions( { headers: headers } );
        // let twilioUrl = "https://api.twilio.com/2010-04-01/Accounts/AC153f2213d8a52c53533b99bac954afea/Tokens.json"; Anil
        // let twilioUrl = "https://api.twilio.com/2010-04-01/Accounts/AC1548e5e14ab9ef52f9570050c0a29e8d/Tokens.json";
        let url = this.baseUrl + 'getIceServer';
        return this.http.post( url , msgBody, {})
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }

    logout() {
        let promise = new Promise( (resolve, reject) => {

            this.postData( 'userlogout').subscribe(
                    result => {
                        resolve(result);
                    },
                    error => {
                        reject(error);
                    }
                );
        });
        return promise;
    }
}
