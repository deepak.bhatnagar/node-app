import { Injectable,Inject , EventEmitter } from '@angular/core';

import * as io from 'socket.io-client';

import { Config } from '../app.config';
import { My, Users, Groups } from '../app.data';

@Injectable()
export class ChatService {

    session:any;

    emitSelfUpdate: EventEmitter<any> = new EventEmitter<any>();
    emitUpdateUsers: EventEmitter<any> = new EventEmitter();
    emitUpdateGroup: EventEmitter<any> = new EventEmitter();
    emitUserOnline: EventEmitter<any> = new EventEmitter<any>();
    emitUserOffline: EventEmitter<any> = new EventEmitter<any>();
    emitMessageUser: EventEmitter<any> = new EventEmitter<any>();
    emitMessageGroup: EventEmitter<any> = new EventEmitter<any>();
    emitCallRequest: EventEmitter<any> = new EventEmitter<any>();
    emitCallResponse: EventEmitter<any> = new EventEmitter<any>();

    private url = Config.chatEndpoint;
    private socket;

    selfUpdate() { return this.emitSelfUpdate; }
    updateUsers() { return this.emitUpdateUsers; }
    onlineUserEvent() { return this.emitUserOnline; }
    offlineUserEvent() { return this.emitUserOffline; }
    updateGroup() { return this.emitUpdateGroup; }
    onUserMessage() { return this.emitMessageUser; }
    onGroupMessage() { return this.emitMessageGroup; }
    onCallRequest() { return this.emitCallRequest }
    onCallResponse() { return this.emitCallResponse }

    // emitCallDisconnect: EventEmitter<any> = new EventEmitter<any>();
    // callDisconnectEvent() { return this.emitCallDisconnect }

    connectSocket() {

        if (this.socket) {
            return this.socket;
        }

        // let obj = param ? { query: "param="+JSON.stringify(param) }  : {} ;
        console.log('this.url',this.url);
        this.socket = io.connect( this.url );

        this.socket.on('connect', () => {
            console.log('connected', this.socket.id);
            this.socket.emit('user.join', { userId : My.getUserId() });
        });
        this.socket.on('connect_error', (e) =>{
            console.error('connect_error',e);
        });
        this.socket.on('message.user', ( data) =>{
            if (data.from != My.getUserId()) {
                Users.unreadPlus(data.from);
            }
            this.emitMessageUser.emit( data );
            this.emitUpdateUsers.emit();
        });
        this.socket.on('message.group', (data) =>{
            if (data.from != My.getUserId()) {
                Groups.unreadPlus(data.to);
            }
            this.emitMessageGroup.emit( data );
            this.emitUpdateGroup.emit();
        });
        this.socket.on('user.online', (user) => {
            console.log('user.online',user);
            Users.addEdit(user);
            // this.emitUserOnline.emit(user);
            this.emitUpdateUsers.emit();
        });
        this.socket.on('user.offline', (user) => {
            // Users.offline(user.userid);
            console.log('user.offline',user);
            Users.addEdit(user);
            // this.emitUserOffline.emit(user);
            this.emitUpdateUsers.emit();
        });
        this.socket.on('user.updated', (data) => {
            if ( My.getUserId() == data.userId ) {
                this.emitSelfUpdate.emit(data);
            } else {
                console.log('Users.addEdit', data);
                Users.addEdit(data);
                this.emitUpdateUsers.emit();
            }
        });
        this.socket.on('call.request', (data) => {
            if(My.getUserId() != data.from) {
                this.emitCallRequest.emit(data);
            }
        });
        // this.socket.on('call.disconnect', (data) => {
        //     if(My.getUserId() != data.from) {
        //         this.emitCallDisconnect.emit(data);
        //     }
        // });
        this.socket.on('call.response', (data) => {
            this.emitCallResponse.emit(data);
        });
        this.socket.on('group.created', (data) => {
            console.log('group.created', data);
            data.unread = 0;
            Groups.addEdit(data);
            this.emitUpdateGroup.emit();
        });
        this.socket.on('group.updated', (data) => {
            console.log('group.updated', data);
            data.unread = 0;
            Groups.addEdit(data);
            this.emitUpdateGroup.emit();
        });
        this.socket.on('group.joined', (data) => {
            data.unread = 0;
            console.log('group.joined', data);
            Groups.addEdit(data);
            this.emitUpdateGroup.emit();
        });
        this.socket.on('group.removed', (data) => {
            console.log('group.removed', data);
            Groups.delete(data.groupId);
            this.emitUpdateGroup.emit();
        });
    }

    addGroup(igroup , callback) {
        this.socket.emit('group.create', igroup, callback);
    }
    editGroup(igroup, callback) {
        this.socket.emit('group.update', igroup, callback);
    }

    sendMessageUser(to, data ) {
        let newData = { to: to, type: data.type, message: data.message };
        this.socket.emit('message.user', newData  );
        // let getData = { to:to, fromDate: new Date(2017, 10, 31), toDate : new Date() };
        // this.getUserMessage(getData, (err, results)=>{
        //     console.log('getUserMessage', err, results);
        // });
    }
    sendMessageGroup(to, data ) {
        this.socket.emit('message.group', { to: to, type: data.type, message: data.message }  );
    }
    createRoom(callback ) {
        this.socket.emit('create.room', (roomid) => {
            callback(roomid);
        });
    }
    startCall( data ) {
        this.socket.emit('call.start', data );
    }
    addCall( data ) {
        this.socket.emit('call.add', data);
    }
    callDisconnect( data ) {
        this.socket.emit( 'call.disconnect', data );
    }
    getUserMessage( data , callback ) {
        this.socket.emit( 'get.message.user', data , callback );
    }
    getGroupMessage( data , callback ) {
        this.socket.emit( 'get.message.group', data , callback );
    }
    callResponse( data ) {
        this.socket.emit('call.response', data);
    }
    closeSocket(){
        // if(this.socket) {
            this.socket.close();
        // }
            console.log('closeSocket');
            this.socket = null;
    }
    updateUser(data ,callback) {
        console.log('updateUser',data);
        this.socket.emit('user.update', data, callback);
    }
}