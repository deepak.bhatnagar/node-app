import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormsModule } from '@angular/forms';
import { Validators } from '../validator';
import { UrlService } from '../app.url.service';
import { Config } from '../../app.config';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css'],

})

export class ContactUsComponent implements OnInit {
  isLoading: boolean = false;
  contactForm: FormGroup
  loader: boolean = false;
  errorMsg: any;
  loaderMessage:string = '';
  msgColor:string =  '#28b750';

  constructor(private formBuilder: FormBuilder, private urlService: UrlService) {}

  ngOnInit() {
    this.contactForm = this.formBuilder.group({
      'fullName': ['', Validators.compose([Validators.required, Validators.maxLength(50)])],
      'email': ['', Validators.compose([Validators.required, Validators.maxLength(30), Validators.validateEmail])],
      'message': ['', Validators.compose([Validators.required, Validators.maxLength(300)])],
    });
  }

  onSubmit(form) {
    form.fullName = form.fullName.trim();
    form.email = form.email.trim();
    form.message = form.message.trim();
    form.refferalURL = document.referrer || Config.apiEndpoint;
    this.loader = true;
    this.urlService.postData('contactUs', form).subscribe(
      result => {
        this.loader = false;
        if (result.code == 500) {
          this.errorMsg = result.msg;              
          this.msgColor =  '#d62424';
        } else {
          this.msgColor =  '#28b750';
          this.errorMsg = result.msg;       
          this.contactForm.reset();          
          console.log("result",result);
        }
        setTimeout(() => {
          this.errorMsg = '';
        }, 5000);
      },
      error => {
        this.msgColor =  '#d62424';
        console.log(error);
      })
  }//onSubmit

}
