import { Component, OnInit } from '@angular/core';

declare var $: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor() {
    this.showHeader();
  }

  ngOnInit() {
    $("#about-us").click(function (){
      $('html, body').animate({
                  scrollTop: $("#about-us-section").offset().top-50
              },800);
          });
  }

  showHeader() {
    let shrinkHeader = 100;

    $(window).scroll(function () {
      if(!document.querySelector(".main-header")){
        return false;
      }
      var scroll = getCurrentScroll();
      if (scroll >= shrinkHeader) {
        let classList = document.querySelector(".main-header").classList;        
        classList.toggle("shrink",true);
        let img:any = document.querySelector(".navbar-brand > img");
        img.src = 'assets/images/logo.png';
        //$('.main-header').addClass('shrink');
        //$(".navbar-brand > img").attr("src", "assets/images/logo.png");
      }else {
        let classList = document.querySelector(".main-header").classList;        
        classList.toggle("shrink",false);
        let img:any = document.querySelector(".navbar-brand > img");
        img.src = 'assets/images/logo-white.png';
        //$('.main-header').removeClass('shrink');
        //$(".navbar-brand > img").attr("src", "assets/images/logo-white.png");
      }
    });
    function getCurrentScroll() {
      return window.pageYOffset || document.documentElement.scrollTop;
    }
  }//showHeader

  scrollTo(id){   
    let elmnt:any = document.querySelector(id);
   // elmnt.scrollIntoView();
    window.scroll({
      top: elmnt.offsetTop,//2100, 
      left: 0, 
      behavior: 'smooth' 
    });
  }//showHeader

}
