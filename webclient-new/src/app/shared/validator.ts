import { ValidatorFn, AsyncValidatorFn, FormGroup, Validators as V, FormControl, FormArray } from '@angular/forms';

// the need in this validators is the non-trimming angular standard behavior
// see https://github.com/angular/angular/issues/8503
export class Validators {

  public static required(control: FormControl) {
    if (!control.value || typeof control.value === 'string' && !control.value.trim()) {
      return {
        required: true
      };
    }

    return null;
  }

  public static minLength(length: number): ValidatorFn {
    return (control: FormControl) => {
      if (!control.value || typeof control.value === 'string' && control.value.trim().length < length) {
        return {
          minlength: true
        };
      }

      return null;
    };
  }

  public static maxLength(length: number): ValidatorFn {
    return (control: FormControl) => {
      if (control.value && typeof control.value === 'string' && control.value.trim().length > length) {
        return {
          maxlength: true
        };
      }

      return null;
    };
  }

  public static pattern(pattern: string): ValidatorFn {
    return V.pattern(pattern);
  }

  public static minAmount(amount: number): ValidatorFn {
    return (control: FormControl) => {
      if (control.value && control.value.length < amount) {
        return {
          minamount: true
        };
      }

      return null;
    };
  }

  public static maxAmount(amount: number): ValidatorFn {
    return (control: FormControl) => {
      if (control.value && control.value.length > amount) {
        return {
          maxamount: true
        };
      }

      return null;
    };
  }

  public static compose(validators: ValidatorFn[]): ValidatorFn {
    return V.compose(validators);
  }

  public static composeAsync(validators: AsyncValidatorFn[]): AsyncValidatorFn {
    return V.composeAsync(validators);
  }
  public static validateEmail(c: FormControl) {
        let EMAIL_REGEXP = new RegExp('[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}');
        return EMAIL_REGEXP.test(c.value) ? null : { validateEmail: { valid: false } };
    }
  
  public static validateAlphabetWtihSpace(c: FormControl) {
      let EMAIL_REGEXP = new RegExp('^[a-zA-Z ]+$');
      return EMAIL_REGEXP.test(c.value) ? null : { validateAlphabetWtihSpace: { valid: false } };
  }
  public static validateAlphabetWtihoutSpace(c: FormControl) {
    let EMAIL_REGEXP = new RegExp('^[a-zA-Z0-9._-]+$');
    return EMAIL_REGEXP.test(c.value) ? null : { validateAlphabetWtihoutSpace: { valid: false } };
  }
  public static validateAlphabetWtihSpace_n_Dash(c: FormControl) {
    let EMAIL_REGEXP = new RegExp('^[a-zA-Z0-9. -]+$');
    return EMAIL_REGEXP.test(c.value) ? null : { validateAlphabetWtihSpace_n_Dash: { valid: false } };
  }
  public static matchingPasswords(passwordKey: string, confirmPasswordKey: string) {
        return (group: FormGroup): { [key: string]: any } => {
            let password = group.controls[passwordKey];
            let confirmPassword = group.controls[confirmPasswordKey];
            if (password.value !== confirmPassword.value) {
                return {
                    mismatchedPasswords: true
                };
            }
        }
  }
  public static multipleCheckboxRequireOne(fa: FormArray) {
    let valid = false;
    for (let x = 0; x < fa.length; ++x) {
      if (fa.at(x).value.isCorrect || fa.at(x).value.isCorrect == undefined) {
        valid = true;
        break;
      }
    }
    return valid ? null : {
      multipleCheckboxRequireOne: true
    };
  }

};