import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoaderComponent } from './loader/loader.component';
import { HeaderComponent } from "./header/header.component";
import { ContactUsComponent } from './contact-us/contact-us.component';
import { FooterComponent } from './footer/footer.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { UrlService } from './app.url.service';
import { RouterModule } from '@angular/router';


@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,RouterModule
  ],
  declarations: [
    LoaderComponent,   
    HeaderComponent, 
    ContactUsComponent,
    FooterComponent 
    
  ],
  exports: [
    LoaderComponent,
    HeaderComponent,
    ContactUsComponent,
    FooterComponent
    
  ],
  providers: [    
    UrlService
],
  entryComponents: [
    
  ]
})
export class SharedModule { }
