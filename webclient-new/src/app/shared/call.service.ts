import { Injectable, Inject , EventEmitter } from '@angular/core';

import * as io from 'socket.io-client';
import { UtilityService } from '../shared/utility.service';
import { Config } from '../app.config';
import { My } from '../app.data';
import * as hark from '../../../node_modules/hark/hark.bundle.js';
declare function openwebrtc(socket: any): any;
declare function openFilterStatsObj(): any;

@Injectable()
export class CallService {
    utils: UtilityService;
    webrtc: any;
    iceServers: Array<any> = [];
    preScreen = { isSupport: false, isPlugin: false, url: '' };
    filterStatsObj = openFilterStatsObj();
    constructor(
            @Inject( UtilityService ) utils: UtilityService,
         ) {
        this.utils = utils;
    }
    emitLocalStream: EventEmitter<any> = new EventEmitter<any>();
    onlocalstream() { return this.emitLocalStream; }

    emitEndLocalStream: EventEmitter<any> = new EventEmitter<any>();
    localstreamEnded() { return this.emitEndLocalStream; }

    emitEndRemoteStream: EventEmitter<any> = new EventEmitter<any>();
    remotestreamEnded() { return this.emitEndRemoteStream; }

    emitRemoteStream: EventEmitter<any> = new EventEmitter<any>();
    onremotestream() { return this.emitRemoteStream; }

    emitpeerDisconnect: EventEmitter<any> = new EventEmitter<any>();
    onPeerDisconnect() { return this.emitpeerDisconnect; }


    emitondevice: EventEmitter<any> = new EventEmitter<any>();
    ondevice() { return this.emitondevice; }

    emitonupdatedata: EventEmitter<any> = new EventEmitter<any>();
    onupdatedata() { return this.emitonupdatedata; }

    emitGetRemote: EventEmitter<any> = new EventEmitter<any>();
    onGetRemotes() { return this.emitGetRemote; }

    // --------------------------------------------------------//
    emitOnSwitching: EventEmitter<any> = new EventEmitter<any>();
    onSwitching() { return this.emitOnSwitching; }

    emitOnNotification: EventEmitter<any> = new EventEmitter<any>();
    onNotification() { return this.emitOnNotification; }

    emitMessageFile: EventEmitter<any> = new EventEmitter<any>();
    onmessageFile() {return this.emitMessageFile; }


    emitonStatistics: EventEmitter<any> = new EventEmitter<any>();
    onStatistics() {return this.emitonStatistics; }

    emitStopStates: EventEmitter<any> = new EventEmitter<any>();
    stopStates() {return this.emitStopStates; }

    emitMessageWhiteBoard: EventEmitter<any> = new EventEmitter<any>();
    onmessageWhiteBoard() {return this.emitMessageWhiteBoard; }


    emitUserTyping: EventEmitter<any> = new EventEmitter<any>();
    onTyping() {return this.emitUserTyping; }

    emitIsUserTalking: EventEmitter<any> = new EventEmitter<any>();
    onSpeak() { return this.emitIsUserTalking; }
    // --------------------------------------------------------//

    emitMessage: EventEmitter<any> = new EventEmitter<any>();
    onmessage() { return this.emitMessage; }

    setRtcEvents(callback) {
        if (this.webrtc) {
            callback(this.webrtc);
            return;
        }
        this.webrtc = openwebrtc(io);
        this.webrtc.DataChannel = true;
        this.webrtc.video =  My.getVideoCall();
        this.webrtc.socketUrl = Config.callEndpoint;
        this.webrtc.iceServers = this.iceServers;
        this.webrtc.data.userId = My.getUserId();
        this.webrtc.data.profilePath = My.getProfilePath();
        this.webrtc.data.displayName = My.getDisplayName();
        this.webrtc.data.isGuest = My.isGuest();
        console.log('session', this.webrtc.data);
        this.webrtc.ondevice = ( dv ) => {
            console.log('getDevices', dv);
            this.emitondevice.emit(dv);
        };
        this.webrtc.onlocalstream =  (event) => {
            let data  = this.filterStream(event);
            this.emitLocalStream.emit(data);
            console.log('onlocalstream', data);
        };
        this.webrtc.onremotestream =  (event) => {
            let data  = this.filterStream(event);
            this.emitRemoteStream.emit(data);
            console.log('onremotestream', data);
        };
        this.webrtc.onpeerdisconnect = (event) => {
            this.emitpeerDisconnect.emit(event);
            console.log('onpeerdisconnect', event);
        };
        this.webrtc.onLocalStreamended = (data) => {
            this.emitEndLocalStream.emit(data);
            console.log('onLocalStreamended', data);
        };
        this.webrtc.onRemoteStreamended = (data) => {
            this.emitEndRemoteStream.emit(data);
            console.log('onRemoteStreamended', data);
        };
        this.webrtc.onupdatedata = (data) => {
            this.emitonupdatedata.emit(data);
            console.log('onupdatedata', data);
        };
        this.webrtc.onGetRemotes = () => {
            this.emitGetRemote.emit();
        };

        // ------------------------------------------------------------------------------------------------------------------------//
        this.webrtc.onSwitching = (data) => {
            this.emitOnSwitching.emit(data);
        };

        this.webrtc.onmessageFile = (data) => {
            this.emitMessageFile.emit(data);
        };

        this.webrtc.onStatistics = (tempData, result) => {
            // console.log("result",  result);
            var statData = {
                bandwidth_persecond : this.filterStatsObj.bytesToSize(result.bandwidth_persecond) + ' / sec',
                key : tempData.socketId + tempData.streamid,
                streamid : tempData.streamid,
                socketId: tempData.socketId,
                type: tempData.type,
                isScreen : tempData.isScreen,
                byteSent_persecond : this.filterStatsObj.bytesToSize(result.byteSent_persecond) + ' / s',
                bytesReceived_persecond: this.filterStatsObj.bytesToSize(result.bytesReceived_persecond) + ' / s',
                // resolution : {
                //     send : result.resolutions.send.width + 'x' + result.resolutions.send.height,
                //     receive : result.resolutions.recv.width + 'x' + result.resolutions.recv.height
                // },
                codecs : {
                    audio : {
                        send: result.audio.send.codecs,
                        rec: result.audio.recv.codecs
                    },
                    video : {
                        send : result.video.send.codecs,
                        rec : result.video.recv.codecs
                    }
                },
                // ip : {
                //     send : result.connectionType.local.ipAddress.join(', '),
                //     receive : result.connectionType.remote.ipAddress.join(', ')
                // },
                result : result,
                totalBytesReceived : result.totalBytesReceived,
                totalBytesSend : result.totalBytesSend,
                audioOutputLevel : result.audioOutputLevel,
                connectionType : {
                    local : result.connectionType.local,
                    remote : result.connectionType.remote
                },
                googLocalAddress : result.googLocalAddress,
                googRemoteAddress : result.googRemoteAddress,

                packetsSentAudio : result.packetsSentAudio,
                packetsLostSentAudio : result.packetsLostSentAudio,
                packetsRecAudio : result.packetsRecAudio,
                packetsLostRecAudio : result.packetsLostRecAudio,

                packetsSentVideo : result.packetsSentVideo,
                packetsLostSentVideo : result.packetsLostSentVideo,
                packetsRecVideo : result.packetsRecVideo,
                packetsLostRecVideo : result.packetsLostRecVideo,

                // Per Second
                packetsSentAudio_ps : result.packetsSentAudio_ps,
                packetsLostSentAudio_ps : result.packetsLostSentAudio_ps,
                packetsRecAudio_ps : result.packetsRecAudio_ps,
                packetsLostRecAudio_ps : result.packetsLostRecAudio_ps,

                bytesSentAudio_ps : this.filterStatsObj.bytesToSize(result.bytesSentAudio_ps),
                bytesReceivedAudio_ps : this.filterStatsObj.bytesToSize(result.bytesReceivedAudio_ps),

                packetsSentVideo_ps : result.packetsSentVideo_ps,
                packetsLostSentVideo_ps : result.packetsLostSentVideo_ps,
                packetsRecVideo_ps : result.packetsRecVideo_ps,
                packetsLostRecVideo_ps : result.packetsLostRecVideo_ps,

                bytesSentVideo_ps : this.filterStatsObj.bytesToSize(result.bytesSentVideo_ps),
                bytesReceivedVideo_ps : this.filterStatsObj.bytesToSize(result.bytesReceivedVideo_ps),
            };
            this.emitonStatistics.emit(statData);
            // getStatsObj(data , (result)=>{
            //     var statData = {
            //         key : tempData.socketId+tempData.streamid,
            //         streamid : tempData.streamid,
            //         socketId:tempData.socketId,
            //         type: tempData.type,
            //         isScreen : tempData.isScreen,
            //         data : {
            //             // send : filterStatsObj.bytesToSize(this.tmpBytes.send===0?result.audio.bytesSent + result.video.bytesSent:(result.audio.bytesSent + result.video.bytesSent)-this.tmpBytes.send)+' / sec',
            //             // receive : filterStatsObj.bytesToSize(this.tmpBytes.rec===0?result.audio.bytesReceived + result.video.bytesReceived:(result.audio.bytesReceived + result.video.bytesReceived)-this.tmpBytes.rec)+' / sec'
            //             send : filterStatsObj.bytesToSize(result.audio.bytesSent + result.video.bytesSent)+' / sec',
            //             receive : filterStatsObj.bytesToSize(result.audio.bytesReceived + result.video.bytesReceived)+' / sec'
            //         },
            //         resolution : {
            //             send : result.resolutions.send.width + 'x' + result.resolutions.send.height,
            //             receive : result.resolutions.recv.width + 'x' + result.resolutions.recv.height
            //         },
            //         codecs : {
            //             send : result.audio.send.codecs.concat(result.video.send.codecs).join(', '),
            //             receive : result.audio.recv.codecs.concat(result.video.recv.codecs).join(', ')
            //         },
            //         bandwidth : {
            //             // speed : filterStatsObj.bytesToSize(this.tmpBandwidth===0?result.bandwidth.speed:result.bandwidth.speed-this.tmpBandwidth),
            //             speed : filterStatsObj.bytesToSize(result.bandwidth.speed),
            //         },
            //         ip : {
            //             send : result.connectionType.local.ipAddress.join(', '),
            //             receive : result.connectionType.remote.ipAddress.join(', ')
            //         },
            //         result : result,
            //         totalBytesReceived : result.totalBytesReceived,
            //         totalBytesSend : result.totalBytesSend,
            //         audioInputLevel : result.audioInputLevel
            //     };

            //     // this.tmpBandwidth = result.bandwidth.speed;
            //     this.tmpBytes.send = result.audio.bytesSent + result.video.bytesSent;
            //     this.tmpBytes.rec = result.audio.bytesReceived + result.video.bytesReceived;

            //     this.emitonStatistics.emit(statData);
            // });
        };
        this.webrtc.stopStates = (tempData) => {
            this.emitStopStates.emit(tempData);
        };
        this.webrtc.onmessageWhiteBoard = (data) => {
            this.emitMessageWhiteBoard.emit(data);
        };
        // ------------------------------------------------------------------------------------------------------------------------//

        this.webrtc.onNotification = (data) => {
            this.emitOnNotification.emit(data);
        };
        this.webrtc.onmessage = (data) => {
            this.emitMessage.emit(data);
        };
        this.webrtc.onTyping = (data) => {
            this.emitUserTyping.emit(data);
        };

        this.webrtc.getPreScreen((preScreen) => {
            this.preScreen = preScreen;
        });
        callback(this.webrtc);
    }
    getPreScreen(): any {
        console.log('getPreScreen', this.preScreen);
        return this.preScreen;
    }
    isExistRoom( roomId, callback ) {
        this.setRtcEvents(() => {
            this.webrtc.isExistRoom( roomId , callback );
        });
    }

    openRoom(roomid , callback) {
        this.setRtcEvents((webrtc) => {
            this.webrtc.roomid = roomid;
            this.webrtc.video =  My.getVideoCall();
            this.webrtc.queryParams = My.getQueryParams()
            setTimeout(() => {
                this.webrtc.joinroom(callback);
            }, 100);
        });
    }
    closeRoom() {
        if (this.webrtc) {
            this.webrtc.closeRoom();
            this.webrtc = null;
        }
    }
    filterStream(event) {
        let data  = JSON.parse(JSON.stringify(event));
        data.socketid = data.id;
        delete data.stream;
        delete data.id;
        event.stream.data = data.data;
        event.stream.userId = data.data.userId;
        event.stream.socketid = data.socketid;
        return event.stream;
    }
    shareScreen(callback) {
        this.webrtc.shareScreen(callback);
    }
    countRemoteUsers(): number {
        let count = 0 ;
        if (this.webrtc) {
            count = this.webrtc.countRemoteUsers();
        } else {
            console.error('check this later');
        }
        console.log('countRemoteUsers', count);
        return count;
    }
    removeStream(streamid: string) {
        this.webrtc.removeStream(streamid);
    }
    switchDevice(device, callback) {
        this.webrtc.switchDevice(device, callback);
    }
    getStreamStatus(streamid) {
        if (this.webrtc.data.streamStatus[streamid]) {
            return this.webrtc.data.streamStatus[streamid];
        } else {
            console.error(streamid + ' stream id not found');
        }
    }
    getRemoteUsers(sockeid?: string) {
        if (sockeid) {
            return this.webrtc.getRemoteUsers(sockeid);
        } else {
            return this.webrtc.getRemoteUsers();
        }
    }
    setStreamStatus(streamid , status, callback) {
        this.webrtc.setStreamStatus(streamid , status, callback);
    }
    updateData( data , callback) {
        for (var key in data) {
            if (data.hasOwnProperty(key)) {
                this.webrtc.data[key] = data[key];
            }
        }

        this.webrtc.updatedata(callback);
    }
    synData(callback) {
        this.webrtc.updatedata(callback);
    }
    setIceServers(iceServers) {
        this.iceServers = iceServers;
    }
    sendTextMessage(msg) {
        this.webrtc.sendTextMessage(msg);
    }
    // ------------------------------------//
    sendNotification(data) {
        this.webrtc.sendNotification(data);
    }
    sendFileMessage(file) {
        this.webrtc.sendFileMessage(file);
    }
    changeBandWidth(streamid , quality) {
        this.webrtc.changeBandWidth(streamid , quality);
    }
    setAudioBandwidthForAll(noBytes) {
        this.webrtc.setAudioBandwidthForAll(noBytes)
    }

    // sending typing event to socket
    getTypingUser(user) {
        this.webrtc.userTyping(user);
    }
    // get current remote speakers
    getCurrentSpeaker(stream) {
        let options = {};

        var speechEvents = hark(stream, options);
        speechEvents.on('speaking', () => {
            let param = {
                streamId: stream.id,
                displayName: stream.data.displayName,
                isUserSpeaking: true
            };
            this.emitIsUserTalking.emit( param );
        });
        speechEvents.on('stopped_speaking', () => {
            let param = {
                streamId: stream.id,
                displayName : stream.data.displayName,
                isUserSpeaking : false
            };
            this.emitIsUserTalking.emit( param );
        });

    }

    sendWhiteBoard(data) {
        this.webrtc.sendWhiteBoard(data);
    }
    // ------------------------------------//

    getDevices(callback) {
        if (!navigator.mediaDevices || !navigator.mediaDevices.enumerateDevices) {
            return callback ("enumerateDevices() not supported in your browser.");
        }
        
        navigator.mediaDevices.enumerateDevices()
        .then((devices) => {
            const dv = {}
            devices.forEach(device => {
                if (!dv[device.kind]) {
                    dv[device.kind] = []
                }
                dv[device.kind].push(device)
            });
            callback(null, dv)
        }).catch((err) => {
            callback(err.name + ": " + err.message);
        });
    }
}
