import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes, Router } from '@angular/router';
import { AboutUsComponent } from './about-us.component';

const appRoutes: Routes = [
   { path: '', component: AboutUsComponent }
];

export const AboutUsRouting: ModuleWithProviders = RouterModule.forChild( appRoutes );
