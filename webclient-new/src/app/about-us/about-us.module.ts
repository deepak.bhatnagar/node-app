import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AboutUsRouting } from './about-us.routing';
import { AboutUsComponent } from './about-us.component';

@NgModule({
    imports: [
        AboutUsRouting,
        CommonModule,
    ],
    declarations: [ AboutUsComponent ],
    providers: []
})

export class AboutUsModule { }



