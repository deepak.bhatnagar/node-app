import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes, Router } from '@angular/router';
import { ContactUsComponent } from './contact-us.component';

const appRoutes: Routes = [
   { path: '', component: ContactUsComponent }
];

export const ContactUsRouting: ModuleWithProviders = RouterModule.forChild( appRoutes );
